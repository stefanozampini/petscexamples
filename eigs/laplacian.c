/*
  Solve a linear eigenvalue problem using SLEPc.

  Here we solve for the second smallest eigenvalue of the graph Laplacian (https://en.wikipedia.org/wiki/Laplacian_matrix) of a mesh.
  The Laplacian matrix of a graph G(V,E) is defined as

              / degree(v_i) if i == j
    L_{i,j} = | -1 if v_i is connected to v_j
              \ 0 otherwise

  where v_i it the i-th node of the graph.

  We use PETSc's DMPLEX class to handle the mesh, and SLEPc's EPS to solve the eigenproblem.

  We also describe how to use the DM for visualization purposes.

  The second smallest eigenvalue, a.k.a the Fiedler's vector (https://en.wikipedia.org/wiki/Algebraic_connectivity), can be exported
  for visualization in Paraview using '-eig0_view vtk:fiedler.vtu:vtk_vtu'

*/

/* Include SLEPc's EPS and its dependencies */
#include <slepceps.h>

/* Include PETSc's DMPLEX API and its dependencies */
#include <petscdmplex.h>

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/*
  Create a mesh with command line options facilities, for example:

     - A triangle box mesh: -mesh_dm_plex_shape box -mesh_dm_plex_cell triangle -mesh_dm_plex_dim 2
     - An hexadral box mesh: -mesh_dm_plex_shape box -mesh_dm_plex_cell hexahedron -mesh_dm_plex_dim 3 -mesh_dm_plex_simplex 0
     - A nicely refined sphere: -mesh_dm_plex_shape sphere -mesh_dm_refine_pre 4
     - A Gyroid: -mesh_dm_plex_shape gyroid -mesh_dm_refine 2 -mesh_dm_plex_tps_layers 1 -mesh_dm_plex_tps_thickness 0.1 -mesh_dm_plex_tps_extent 2,2,2
     - From filename (GMSH): -mesh_dm_plex_filename <somegmshfile>

  Run with '-help' and grep for 'mesh_dm_' for additional options

  The mesh can be exported for visualization in Paraview using '-mesh_view vtk:mesh.vtu:vtk_vtu'

*/
PetscErrorCode LoadMesh(DM *odm)
{
  DM dm;

  PetscFunctionBeginUser;
  PetscCall(DMCreate(PETSC_COMM_WORLD, &dm));
  PetscCall(DMSetOptionsPrefix(dm, "mesh_"));
  PetscCall(DMSetType(dm, DMPLEX));
  PetscCall(DMSetFromOptions(dm));
  PetscCall(DMLocalizeCoordinates(dm));

  /* Allow command line view via -mesh_view */
  PetscCall(DMViewFromOptions(dm, NULL, "-view"));

  /* Return mesh to caller */
  *odm = dm;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/*
   Computes the graph laplacian L of a mesh, stored in the DMPLEX format.
      L = D - A, with D = degree matrix and A = adjacency matrix
*/
PetscErrorCode ComputeLaplacian(DM dm, Mat *oL)
{
  Mat          L, preall;
  Vec          x, y;
  PetscInt    *i, *j, numVertices, rst, maxnnzrow, dim, *numDof, numFields;
  PetscScalar *vals;
  PetscSection s;

  PetscFunctionBeginUser;
  /* Access CSR graph of local partition */
  PetscCall(DMPlexCreatePartitionerGraph(dm, 0, &numVertices, &i, &j, NULL));

  /* First create a matrix object */
  PetscCall(MatCreate(PetscObjectComm((PetscObject)dm), &L));
  PetscCall(MatSetSizes(L, numVertices, numVertices, PETSC_DECIDE, PETSC_DECIDE));
  PetscCall(MatSetOptionsPrefix(L, "laplacian_"));
  PetscCall(MatSetFromOptions(L));

  /* Preallocation. Here we use an helper class called MATPREALLOCATOR that
     does it for us. We only need to loop once with our matrix insertion loop and
     populate the MATPREALLOCATOR */
  PetscCall(MatCreate(PetscObjectComm((PetscObject)dm), &preall));
  PetscCall(MatSetSizes(preall, numVertices, numVertices, PETSC_DECIDE, PETSC_DECIDE));
  PetscCall(MatSetType(preall, MATPREALLOCATOR));
  PetscCall(MatSetUp(preall));
  PetscCall(MatGetOwnershipRange(preall, &rst, NULL));
  maxnnzrow = 0;
  for (PetscInt k = 0; k < numVertices; k++) {
    PetscInt  nnzrow = i[k + 1] - i[k];
    PetscInt  row    = rst + k;
    PetscInt *col    = j + i[k];

    maxnnzrow = PetscMax(maxnnzrow, nnzrow);

    /* Add adjacency connection */
    PetscCall(MatSetValues(preall, 1, &row, nnzrow, col, NULL, INSERT_VALUES));

    /* The graph CSR does not represent self-to-self connections, we need them
       for the graph laplacian */
    PetscCall(MatSetValues(preall, 1, &row, 1, &row, NULL, INSERT_VALUES));
  }
  PetscCall(MatAssemblyBegin(preall, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(preall, MAT_FINAL_ASSEMBLY));

  /* Preallocate the graph laplacian matrix */
  PetscCall(MatPreallocatorPreallocate(preall, PETSC_TRUE, L));
  PetscCall(MatDestroy(&preall));

  /* Set values. We first set all values to -1.0 to obtain -A,
     and then use matrix API to modify for our needs and add the diagonal D matrix */
  PetscCall(PetscMalloc1(maxnnzrow, &vals));
  for (PetscInt k = 0; k < maxnnzrow; k++) vals[k] = -1.0;

  for (PetscInt k = 0; k < numVertices; k++) {
    PetscInt  nnzrow = i[k + 1] - i[k];
    PetscInt  row    = rst + k;
    PetscInt *col    = j + i[k];

    PetscCall(MatSetValues(L, 1, &row, nnzrow, col, vals, INSERT_VALUES));
  }
  PetscCall(MatAssemblyBegin(L, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(L, MAT_FINAL_ASSEMBLY));

  /* Add D. Here we use the fact that D = rowsum(A) */
  PetscCall(MatCreateVecs(L, &x, &y));
  PetscCall(VecSet(x, -1.0));
  PetscCall(MatMult(L, x, y));
  PetscCall(MatDiagonalSet(L, y, INSERT_VALUES));
  PetscCall(VecDestroy(&x));
  PetscCall(VecDestroy(&y));

  /* clean up */
  PetscCall(PetscFree(vals));
  PetscCall(PetscFree(i));
  PetscCall(PetscFree(j));

  /* Allow command line view via -laplacian_view */
  PetscCall(MatViewFromOptions(L, NULL, "-view"));

  /*
    For visualization purposes, we attach a DM to the matrix.
    Cloning makes a shallow (pointer) copy of the mesh topology and geometry,
    and allows us to consider different discretization spaces.
    In this case, we specify a one-field, cell-centered discretization with a PetscSection object.
  */
  PetscCall(DMClone(dm, &dm));
  PetscCall(DMGetDimension(dm, &dim));
  numFields = 1;
  PetscCall(DMSetNumFields(dm, numFields));
  PetscCall(PetscCalloc1(dim + 1, &numDof));
  numDof[dim] = 1;
  PetscCall(DMPlexCreateSection(dm, NULL, &numFields, numDof, 0, NULL, NULL, NULL, NULL, &s));
  PetscCall(DMSetLocalSection(dm, s));
  PetscCall(PetscSectionDestroy(&s));
  PetscCall(PetscFree(numDof));

  /* Attach the DM to the matrix */
  PetscCall(MatSetDM(L, dm));

  /* the matrix holds a reference to the DM, we can decrease reference counting */
  PetscCall(DMDestroy(&dm));

  /* Return matrix to caller */
  *oL = L;
  PetscFunctionReturn(PETSC_SUCCESS);
}

int main(int argc, char *argv[])
{
  DM       dm;
  Mat      L;
  EPS      eps;
  ST       st;
  KSP      ksp;
  PC       pc;
  Vec      x, xr, xi;
  PetscInt nconv;
  char     opts[64];

  /* We initialize SLEPc, that in turns initializes PETSc for us */
  PetscCall(SlepcInitialize(&argc, &argv, NULL, NULL));

  /* Create the mesh */
  PetscCall(LoadMesh(&dm));

  /* Compute the graph laplacian */
  PetscCall(ComputeLaplacian(dm, &L));
  PetscCall(DMDestroy(&dm));

  /* Create eigensolver context */
  PetscCall(EPSCreate(PetscObjectComm((PetscObject)L), &eps));

  /* Set operators. In this case, it is a standard Hermitian eigenvalue problem */
  PetscCall(EPSSetOperators(eps, L, NULL));
  PetscCall(EPSSetProblemType(eps, EPS_HEP));

  /* Select portion of spectrum. Here we want the second smallest eigenvalue */
  PetscCall(EPSSetWhichEigenpairs(eps, EPS_SMALLEST_REAL));

  /* Attach deflation space: in this case, the matrix has a constant nullspace */
  PetscCall(MatCreateVecs(L, &x, NULL));
  PetscCall(VecSet(x, 1.0));
  PetscCall(EPSSetDeflationSpace(eps, 1, &x));
  PetscCall(VecDestroy(&x));

  /* Choose LOBPCG as a solver and customize linear solver
       A. V. Knyazev, "Toward the optimal preconditioned eigensolver:
       locally optimal block preconditioned conjugate gradient method",
       SIAM J. Sci. Comput. 23(2):517-541, 2001. */
  PetscCall(EPSSetType(eps, EPSLOBPCG));
  PetscCall(EPSGetST(eps, &st));
  PetscCall(STGetKSP(st, &ksp));
  PetscCall(KSPSetType(ksp, KSPCG));
  PetscCall(KSPGetPC(ksp, &pc));
  PetscCall(PCSetType(pc, PCGAMG));

  /* Set solver parameters at runtime */
  PetscCall(EPSSetFromOptions(eps));

  /* Solve the eigenproblem */
  PetscCall(EPSSolve(eps));

  /* Get number of converged solutions */
  PetscCall(EPSGetConverged(eps, &nconv));

  /*
    Allow dumping of vectors from command line options
    For this purpose, we retrieve scratch vectors from the DM.
    These vectors holds the extra mesh information to allow
    integration with external tools, like e.g. Paraview.
  */
  PetscCall(MatGetDM(L, &dm));
  PetscCall(DMGetGlobalVector(dm, &xr));
  PetscCall(DMGetGlobalVector(dm, &xi));
  for (PetscInt i = 0; i < nconv; i++) {
    PetscScalar kr, ki;
    PetscReal   re, im;

    PetscCall(PetscSNPrintf(opts, sizeof(opts), "%d-th Eigenvector ", (int)i));
    PetscCall(PetscObjectSetName((PetscObject)xr, opts));
    PetscCall(PetscSNPrintf(opts, sizeof(opts), "-eig%d_view", (int)i));
    PetscCall(EPSGetEigenpair(eps, i, &kr, &ki, xr, xi));
#if defined(PETSC_USE_COMPLEX)
    re = PetscRealPart(kr);
    im = PetscImaginaryPart(kr);
#else
    re = kr;
    im = ki;
#endif
    PetscCall(PetscPrintf(PetscObjectComm((PetscObject)L), "%d-th Eigenvalue %g + %g i\n", (int)i, (double)re, (double)im));
    PetscCall(VecViewFromOptions(xr, NULL, opts));
  }

  /* Restore the vectors so that the DM can freely destroy them when it is destroyed */
  PetscCall(DMRestoreGlobalVector(dm, &xr));
  PetscCall(DMRestoreGlobalVector(dm, &xi));

  /* Clean up */
  PetscCall(MatDestroy(&L));
  PetscCall(EPSDestroy(&eps));

  PetscCall(SlepcFinalize());
  return 0;
}
