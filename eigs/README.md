This folder contains the following examples:

- [laplacian](laplacian.c): Computes the Fiedler's vector of the graph Laplacian associated with an unstructured mesh.

- [pseudospectral](pseudospectral.py): Matrix-free eigensolver to compute the spectrum of the two-dimensional discretization of the Laplacian operator using Chebyshev polynomials. This has been adapted from Program 23 of Lloyd N. Trefethen's book [Spectral methods in MATLAB](https://epubs.siam.org/doi/book/10.1137/1.9780898719598).
