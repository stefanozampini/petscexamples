# This script reproduces Program 23 of the well-known book "Spectral Methods in MATLAB" by Lloyd N. Trefethen.
# Here we want to compute the eigenvectors associated with the 4 smallest eigenvalues of a perturbed
# two-dimensional Laplacian operator on [-1,1]^2 with zero boundary conditions, discretized using a
# pseudospectral method with Chebyshev polynomials.
#
# This example shows how to construct a custom matrix type in Python using the MATPYTHON type.
# If you are interested in more details, you can find them in
# PETSc's source code at src/binding/petsc4py/src/petsc4py/PETSc/libpetsc4py.pyx
# and look for MatCreate_Python
#
# For more details on the problem at hand, see https://epubs.siam.org/doi/epdf/10.1137/1.9780898719598.ch9

# Initialize SLEPc with command line and import SLEPc and PETSc modules
import sys
import slepc4py

slepc4py.init(sys.argv)
from slepc4py import SLEPc
from petsc4py import PETSc

# We use numpy, scipy and matplotlib
import numpy as np
from scipy.linalg import toeplitz
from scipy.interpolate import griddata
from matplotlib import pyplot as plt


# Computes Chebyshev nodes and differentiation matrices, see ACM TOMS, Vol. 26, pp. 465--519 (2000).
# This code has been adapted from the original MATLAB code by Weidemann and Reddy
# see https://appliedmaths.sun.ac.za/~weideman/research/differ.html
def cheb(n):
    # Chebyshev nodes
    x = np.sin(np.pi * (n - 2 * np.linspace(n, 0, n + 1)) / (2 * n))

    n1 = int(np.floor((n + 1) / 2))
    n2 = int(np.ceil((n + 1) / 2))

    k = np.arange(n + 1)
    th = k * np.pi / n

    T = np.tile(th / 2, (n + 1, 1))
    DX = 2 * np.sin(T.T + T) * np.sin(T.T - T)
    DX[n1:, :] = -np.flipud(np.fliplr(DX[:n2, :]))
    DX[range(n + 1), range(n + 1)] = 1.0
    DX = DX.T

    C = toeplitz((-1.0) ** k)
    C[0, :] *= 2
    C[-1, :] *= 2
    C[:, 0] *= 0.5
    C[:, -1] *= 0.5

    Z = 1.0 / DX
    Z[range(n + 1), range(n + 1)] = 0.0

    # Chebyshev first order differentiation matrix
    D = np.eye(n + 1)
    D = Z * (C * np.tile(np.diag(D), (n + 1, 1)).T - D)
    D[range(n + 1), range(n + 1)] = -np.sum(D, axis=1)

    return x, D


# A Python class that represents our matrix.
# This is the Python equivalent of the MATSHELL type in C/C++
# For our needs, we only need to provide the implementation
# of the matrix-vector multiplication.
class lap:
    def __init__(self, n, pert=None):
        x, D = cheb(n)

        # This is a Dirichlet problem, remove endpoints
        self.n = n - 1
        self.x = x[1:-1]

        self.xx, self.yy = np.meshgrid(self.x, self.x)
        if pert is None:
            pert = lambda x, _y: np.zeros_like(x)
        self.pert = pert

        # Higher-order differentiation matrices can be simply
        # obtained by multiplications
        self.D2 = (D @ D)[1:-1, 1:-1]

    # This is the method that the MATPYTHON class will call when asked to
    # perform MatMult
    def mult(self, _mat, x, y):
        # Wrap PETSc vector data with numpy arrays (no-copy)
        x = x.getArray(readonly=True)
        y = y.getArray()

        # Laplacian * x : - (kron(I, D2) + kron(D2, I))*x
        # recall that kron(A,B) * x = vec ( B * X * A^T) with vec(X) = x
        X = x.reshape(self.n, self.n)
        y[:] = -(self.D2 @ X + X @ self.D2.T).reshape(-1)

        # Add diagonal perturbation
        y += self.pert(self.xx, self.yy).ravel() * x


# Access options database
OptDB = PETSc.Options()
n = OptDB.getInt('n', 16)  # -n <someint> for polynomial degree
usepert = OptDB.getBool('pert', False)  # -pert for solving the perturbed problem
debug = OptDB.getBool('debug', False)  # -debug to dump MATLAB .m file to load the matrix

# Create an instance of our lap class
trefethen_pert = lambda x, y: np.exp(20 * (y - x - 1))
ctx = lap(n, pert=trefethen_pert if usepert else None)

# Create a PETSc matrix of MATPYTHON type, using the instance of 'lap' as context
L = PETSc.Mat().createPython((ctx.n**2, ctx.n**2), ctx, comm=PETSc.COMM_SELF)
L.setUp()

# Convert matrix to aij format (using MatMult internally) and dump in MATLAB format for debug
if debug:
    Ld = L.convert('aij', PETSc.Mat())
    Ld.setName('Lp')
    dviewer = PETSc.Viewer().createASCII('chebylap.m', comm=PETSc.COMM_SELF)
    dviewer.pushFormat(PETSc.Viewer.Format.ASCII_MATLAB)
    Ld.view(dviewer)
    del dviewer
    del Ld

# Create SLEPc eigensolver
eps = SLEPc.EPS().create(comm=PETSc.COMM_SELF)
eps.setOperators(L)

# Use a matrix-free eigensolver
# We use the Arnoldi method since Chebyshev differentiation matrices are not symmetric
eps.setType(SLEPc.EPS.Type.ARNOLDI)

# We mimic Trefethen's MATLAB script and request the 4 smallest eigenvalues
eps.setDimensions(nev=4)
eps.setWhichEigenpairs(SLEPc.EPS.Which.SMALLEST_REAL)

# Allow command line customization
eps.setFromOptions()

# Solve the eigenproblem
eps.solve()

# Get number of converged eigenpairs
nconv = eps.getConverged()
eigs = [f'{eps.getEigenvalue(i).real / (np.pi**2 / 4)} pi^2 / 4' for i in range(nconv)]
PETSc.Sys.Print(f'Converged eigenvalues:\n{eigs}')

# Chebyshev grid
x, _ = cheb(n)
[xx, yy] = np.meshgrid(x, x)

# Finer grid for plotting
[fx, fy] = np.meshgrid(np.linspace(-1.0, 1.0, 100), np.linspace(-1.0, 1.0, 100))

v, _ = L.createVecs()
vv = np.zeros_like(xx)

for i in range(min(nconv, 16)):
    # Get eigenvector
    eps.getEigenvector(i, v)

    # Import data in a numpy array defined on [xx, yy]
    vv[1:-1, 1:-1] = v.getArray(readonly=True).reshape(ctx.n, ctx.n)

    # Interpolate on finer grid and normalize
    vvv = griddata((xx.ravel(), yy.ravel()), vv.ravel(), (fx, fy), method='cubic')
    vvv /= np.linalg.norm(vvv.ravel(), np.inf)

    # Plot
    plt.figure()
    plt.contourf(fx, fy, vvv)
    plt.colorbar()
    plt.title(eigs[i])

plt.show()
