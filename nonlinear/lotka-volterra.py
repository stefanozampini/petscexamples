# This script is the Python version of lotka-volterra.c

# Initialize PETSc with command line and import PETSc module
import sys
import petsc4py

petsc4py.init(sys.argv)
from petsc4py import PETSc


# Here we use callables for RHS function and Jacobian
class RHSFunction:
    def __init__(self, params):
        self.a, self.b, self.c, self.d = params

    def __call__(self, _ts, _t, u, f):
        f[0] = (self.a * u[0] - self.b * u[0] * u[1]).item()
        f[1] = (-self.c * u[1] + self.d * u[0] * u[1]).item()
        f.assemble()


class RHSJacobian:
    def __init__(self, params):
        self.a, self.b, self.c, self.d = params

    def __call__(self, _ts, _t, u, J, P):
        P[0, 0] = (self.a - self.b * u[1]).item()
        P[0, 1] = (-self.b * u[0]).item()
        P[1, 0] = (self.d * u[1]).item()
        P[1, 1] = (-self.c + self.d * u[0]).item()
        P.assemble()
        if J != P:
            J.assemble()


# Access options database
OptDB = PETSc.Options()
params = (
    OptDB.getReal('a', 1.1),
    OptDB.getReal('b', 0.4),
    OptDB.getReal('c', 0.1),
    OptDB.getReal('d', 0.3),
)

# Create vector, matrix and ODE solver
u = PETSc.Vec().createSeq(2)
J = PETSc.Mat().createDense((2, 2), comm=PETSc.COMM_SELF)
J.setUp()
ts = PETSc.TS().create(comm=PETSc.COMM_SELF)

# Pass callbacks to PETSc
ts.setRHSFunction(RHSFunction(params))
ts.setRHSJacobian(RHSJacobian(params), J)

# Set defaults and customize from options
ts.setType(PETSc.TS.Type.RK)
ts.setRKType(PETSc.TS.RKType.RK4)
ts.setMaxTime(100)
ts.setExactFinalTime(PETSc.TS.ExactFinalTime.STEPOVER)
ts.setFromOptions()

# Solve
u.set(0.5)
ts.solve(u)
