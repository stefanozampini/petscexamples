/*
   Solve the Lotka-Volterra nonlinear system of ODEs:

      dx/dt =  a * x - b * x * y
      dy/dt = -c * y + d * x * y

   Here we describe the explicit setup for PETSc's ODE solver TS
   which solves problems of the form

      du/dt = F(t,u)

   In our case, u = [x,y]. A fully implicit setup of the F(t,u,du/dt) = 0
   is also available. See baton.c for details.

   For the python version of this example, see lotka-volterra.py
*/

/* Include PETSc ODE solver API and all its dependencies */
#include <petscts.h>

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* Define a type to hold our parameters */
typedef struct {
  PetscReal a, b, c, d;
} LV;

/*
  This is the callback PETSc will use to sample the right-hand side F(t,u). Inputs are:
  - t the time for sampling
  - U the state vector
  - ctx is the application (user) context passed via TSSetRHSFunction
  Given these values, we need to populate F.

  Such an approach allows to completely decouple the user-defined problem from the solver specifics.

  For example, when using forward Euler, the inputs at the n-th time step would be F(t_n,U_n), while
  for a Runge-Kutta solver will be of the form F(tn + c_i*dt,un + dt*a_i * stages).
  The user does not have to know about these details, and it is only in charge of the input-output callback.
*/
PetscErrorCode RHSFunction(TS ts, PetscReal t, Vec U, Vec F, void *ctx)
{
  const PetscScalar *u;
  PetscScalar       *f;
  LV                *lv = (LV *)ctx;

  PetscFunctionBeginUser;
  /* Tell PETSc we will only read values from U */
  PetscCall(VecGetArrayRead(U, &u));

  /* Tell PETSc we will write values to F */
  PetscCall(VecGetArrayWrite(F, &f));

  /* Evaluate F(t,U) */
  f[0] = lv->a * u[0] - lv->b * u[0] * u[1];
  f[1] = -lv->c * u[1] + lv->d * u[0] * u[1];

  /* Inform PETSc we are done accessing the data */
  PetscCall(VecRestoreArrayRead(U, &u));
  PetscCall(VecRestoreArrayWrite(F, &f));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/*
  This is the callback PETSc will use to sample the Jacobian of the system.

  In our case, it is given by:

            | a-b*y  -b*x   |
    dF/du = |               |
            | d*y    -c+d*x |

  This callback is used in case an implicit solver is employed, like Crank-Nicholson,
  BDF, Theta, etc.

  Inputs to this callback are:
   - t the time used to sample the Jacobian
   - U the state vector
   - ctx the application (user) context passed via TSSetRHSJacobian.
   - A and P the matrices to populate.

  In case an implicit solver is selected, the actual Jacobian used is of the form

      shift * I - RHSJacobian

  where 'shift' is a scalar that depends on the time step and on the specific solver used.
  See e.g. baton.c for an example that uses the implicit formulation.
  Here, PETSc performs the shift internally.

  See also the discussion on A and P in bratu.c for additional details.
*/
PetscErrorCode RHSJacobian(TS ts, PetscReal t, Vec U, Mat A, Mat P, void *ctx)
{
  const PetscScalar *u;
  PetscScalar        v[2][2];
  PetscInt           i[] = {0, 1};
  LV                *lv  = (LV *)ctx;

  PetscFunctionBeginUser;
  /* Evaluate dF/dU */
  PetscCall(VecGetArrayRead(U, &u));
  v[0][0] = lv->a - lv->b * u[1];
  v[0][1] = -lv->b * u[0];
  v[1][0] = lv->d * u[1];
  v[1][1] = -lv->c + lv->d * u[0];
  PetscCall(VecRestoreArrayRead(U, &u));

  /* Insert matrix values */
  PetscCall(MatSetValues(P, 2, i, 2, i, (PetscScalar *)v, INSERT_VALUES));
  PetscCall(MatAssemblyBegin(P, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(P, MAT_FINAL_ASSEMBLY));

  /* If you are experimenting with jacobian testing, try
     - commenting any of the v[][] assignments above
     - commenting out the next call to see the shift value
  */
  // PetscCall(MatZeroEntries(P));

  if (A != P) { /* This is true when we use matrix-free or finite differencing */
    PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

int main(int argc, char *argv[])
{
  TS  ts;
  Mat A;
  Vec U;
  LV  appCtx;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  /* Some default parameters and their command line options */
  appCtx.a = 1.1;
  appCtx.b = 0.4;
  appCtx.c = 0.1;
  appCtx.d = 0.3;
  PetscCall(PetscOptionsGetReal(NULL, NULL, "-a", &appCtx.a, NULL));
  PetscCall(PetscOptionsGetReal(NULL, NULL, "-b", &appCtx.b, NULL));
  PetscCall(PetscOptionsGetReal(NULL, NULL, "-c", &appCtx.c, NULL));
  PetscCall(PetscOptionsGetReal(NULL, NULL, "-d", &appCtx.d, NULL));

  /* Create vector and matrix to hold state and Jacobian */
  PetscCall(VecCreateSeq(PETSC_COMM_SELF, 2, &U));
  PetscCall(VecSet(U, 0.5)); /* initial conditions */
  PetscCall(MatCreateSeqDense(PETSC_COMM_SELF, 2, 2, NULL, &A));

  /* Create ODE solver context */
  PetscCall(TSCreate(PETSC_COMM_SELF, &ts));

  /* Inform PETSc how to compute the residual of the ODE */
  PetscCall(TSSetRHSFunction(ts, NULL, RHSFunction, &appCtx));

  /* Inform PETSc how to compute the Jacobian.

     The latter can be omitted since PETSc supports finite-differencing
     and matrix-free approximations, see bratu.c for additional information.
     Run with

       '-ts_type beuler -ts_dt 1.0 -snes_test_jacobian -snes_test_jacobian_view -ts_max_steps 1 -snes_max_it 1'

     to check the correctness of the user-provided RHSJacobian function.
     Here we use '-ts_type beuler -ts_dt 1.0' to help debugging, since we know in this case the solver shift is 1.
  */
  PetscCall(TSSetRHSJacobian(ts, A, A, RHSJacobian, &appCtx));
  PetscCall(MatDestroy(&A));

  /* Set default solvers and final time (can be overridden at command line)
     Here we use a 4th order Runge-Kutta solver (ode45 in MATLAB).
  */
  PetscCall(TSSetType(ts, TSRK));
  PetscCall(TSRKSetType(ts, TSRK4));
  PetscCall(TSSetMaxTime(ts, 100));
  PetscCall(TSSetExactFinalTime(ts, TS_EXACTFINALTIME_STEPOVER));

  /* Allow command-line customization. Run

       ./lotka-volterra -help | grep '-ts_'

     to see some of the available options.

     This folder also contains a custom ODE solver written in Python, available via:

        '-python -ts_type python -ts_python_type myts.tsodeint'

     If you have an X client available, run with:
       - '-ts_monitor_lg_solution' to plot the solution as time evolves
       - '-ts_monitor_draw_solution_phase 0,0,10,10' to plot the phase diagram
  */
  PetscCall(TSSetFromOptions(ts));

  /* Solve the ODE */
  PetscCall(TSSolve(ts, U));

  /* Cleanup */
  PetscCall(TSDestroy(&ts));
  PetscCall(VecDestroy(&U));
  PetscCall(PetscFinalize());
  return 0;
}
