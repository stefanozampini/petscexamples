/*
   This example demonstrates the use of the TAO package to solve an unconstrained minimization problem.

      argmin f(x), x in R^n
         x

   where f : R^n -> R is given by the extended Rosenbrock's function

      f(x) = sum_{i=0}^{n/2-1} (alpha*(x_{2i+1}-x_{2i}^2)^2 + (1-x_{2i})^2)

*/

/* Include PETSc optimization solver API and all its dependencies */
#include <petsctao.h>

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* Define a type to hold our parameters */
typedef struct {
  PetscReal alpha;
} AppCtx;

/*
  This is the callback PETSc will use to sample the objective function f(x)
  X is the current state vector, ctx is the application (user) context.
*/
PetscErrorCode ComputeObjective(Tao tao, Vec X, PetscReal *f, void *ctx)
{
  AppCtx            *user = (AppCtx *)ctx;
  PetscInt           n;
  PetscReal          ff = 0, alpha = user->alpha;
  const PetscScalar *x;

  PetscFunctionBeginUser;
  PetscCall(VecGetLocalSize(X, &n));
  PetscCall(VecGetArrayRead(X, &x));
  for (PetscInt i = 0; i < n / 2; i++) {
    PetscReal t1 = x[2 * i + 1] - x[2 * i] * x[2 * i];
    PetscReal t2 = 1. - x[2 * i];

    ff += alpha * t1 * t1 + t2 * t2;
  }
  PetscCall(VecRestoreArrayRead(X, &x));
  *f = ff;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/*
  This is the callback PETSc will use to sample the gradient df/dx of the objective function
  The gradient is a vector where the i-th component is df/dx_i
  X is the current state vector, ctx is the application (user) context.
*/
PetscErrorCode ComputeGradient(Tao tao, Vec X, Vec G, void *ctx)
{
  AppCtx            *user = (AppCtx *)ctx;
  PetscInt           n;
  PetscReal          alpha = user->alpha;
  const PetscScalar *x;
  PetscScalar       *g;

  PetscFunctionBeginUser;
  PetscCall(VecGetLocalSize(X, &n));
  PetscCall(VecGetArrayRead(X, &x));
  PetscCall(VecGetArrayWrite(G, &g));
  for (PetscInt i = 0; i < n / 2; i++) {
    PetscReal t1 = x[2 * i + 1] - x[2 * i] * x[2 * i];
    PetscReal t2 = 1. - x[2 * i];

    g[2 * i]     = -4. * alpha * t1 * x[2 * i] - 2.0 * t2;
    g[2 * i + 1] = 2. * alpha * t1;
  }
  PetscCall(VecRestoreArrayWrite(G, &g));
  PetscCall(VecRestoreArrayRead(X, &x));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/*
  Second-order solvers need to know the Hessian of the objective function, which is a matrix
  with entries defined by d^2 f / dx_i dx_j
  X is the current state vector, ctx is the application (user) context.
  H and Hp the matrices to populate.
*/
PetscErrorCode ComputeHessian(Tao tao, Vec X, Mat H, Mat Hp, void *ctx)
{
  AppCtx            *user = (AppCtx *)ctx;
  PetscInt           ind[2], n;
  PetscReal          alpha = user->alpha;
  PetscScalar        v[2][2];
  const PetscScalar *x;

  PetscFunctionBeginUser;
  PetscCall(VecGetLocalSize(X, &n));
  PetscCall(VecGetArrayRead(X, &x));
  for (PetscInt i = 0; i < n / 2; i++) {
    v[1][1] = 2. * alpha;
    v[0][0] = -4. * alpha * (x[2 * i + 1] - 3. * x[2 * i] * x[2 * i]) + 2.;
    v[0][1] = -4.0 * alpha * x[2 * i];
    v[1][0] = v[0][1];
    ind[0]  = 2 * i;
    ind[1]  = 2 * i + 1;
    PetscCall(MatSetValues(Hp, 2, ind, 2, ind, (PetscScalar *)v, INSERT_VALUES));
  }
  PetscCall(VecRestoreArrayRead(X, &x));
  PetscCall(MatAssemblyBegin(Hp, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(Hp, MAT_FINAL_ASSEMBLY));
  if (H != Hp) { /* This is true when we use matrix-free or finite differencing */
    PetscCall(MatAssemblyBegin(H, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(H, MAT_FINAL_ASSEMBLY));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

int main(int argc, char **argv)
{
  Tao      tao;
  Vec      x;
  Mat      H;
  PetscInt n;
  AppCtx   user;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  /* Some default parameters and their command line options */
  n          = 2;
  user.alpha = 99.0;
  PetscCall(PetscOptionsGetInt(NULL, NULL, "-n", &n, NULL));
  PetscCall(PetscOptionsGetReal(NULL, NULL, "-alpha", &user.alpha, NULL));

  /* Allocate Vec to hold the solution */
  PetscCall(VecCreateSeq(PETSC_COMM_SELF, n, &x));

  /* Create matrix to hold the sparse Hessian of this problem */
  PetscCall(MatCreateSeqAIJ(PETSC_COMM_SELF, n, n, 2, NULL, &H));

  /* Create optimization solver context */
  PetscCall(TaoCreate(PETSC_COMM_SELF, &tao));

  /* Inform PETSc how to compute objective, gradient and Hessian.
     Gradient and Hessian can be also approximated by finite-differencing and
     matrix free approximations. Run with

       '-tao_test_gradient -tao_test_hessian'

     to check the correctness of the user-provided functions */
  PetscCall(TaoSetObjective(tao, ComputeObjective, &user));
  PetscCall(TaoSetGradient(tao, NULL, ComputeGradient, &user));
  PetscCall(TaoSetHessian(tao, H, H, ComputeHessian, &user));
  PetscCall(MatDestroy(&H));

  /* Set default solvers (can be overridden at command line)
     Here we select Newton's method as our default.
     Run

       ./rosenbrock -help | grep '-tao_'

     to see some of the available options
  */
  PetscCall(TaoSetType(tao, TAOBNLS));
  PetscCall(TaoSetFromOptions(tao));

  /* Set solution with zero initial guess */
  PetscCall(VecSet(x, 0.0));
  PetscCall(TaoSetSolution(tao, x));

  /* Solve the minimization problem
     You can monitor the value of the objective function as iterations
     proceed by using '-tao_monitor' */
  PetscCall(TaoSolve(tao));

  /* Cleanup */
  PetscCall(TaoDestroy(&tao));
  PetscCall(VecDestroy(&x));
  PetscCall(PetscFinalize());
  return 0;
}
