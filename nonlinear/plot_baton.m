% This is a visualization script taken from
% https://www.mathworks.com/help/matlab/math/solve-equations-of-motion-for-baton.html
% Data must have been generated with
% $ ./baton -view_data :baton.m:ascii_matlab
% For Python, see plot_baton.py

clear all
close all
figure
title('Motion of a Thrown Baton');
baton
L=1
axis([0 22 0 25])
hold on
[n,~] = size(q)
for j = 1:n
   theta = q(j,5);
   X = q(j,1);
   Y = q(j,3);
   xvals = [X X+L*cos(theta)];
   yvals = [Y Y+L*sin(theta)];
   plot(xvals,yvals,xvals(1),yvals(1),'ro',xvals(2),yvals(2),'go')
end
hold off
