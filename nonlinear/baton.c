/*
   See https://www.mathworks.com/help/matlab/math/solve-equations-of-motion-for-baton.html

   Solves the nonlinear system of ODEs:

        M(u) * du/dt = G(t,u) where u = [u_0, u_1, u_2, u_3, u_4, u_5]

               | 1     0       0     0        0      0         |
               | 0   m1+m2     0     0        0 -m2*L*sin(u_4) |
       M(u) =  | 0     0       1     0        0      0         |
               | 0     0       0   m1+m2      0  m2*L*cos(u_4) |
               | 0     0       0     0        1      0         |
               | 0 -L*sin(u_4) 0 L*cos(u_4)   0     L^2        |

               |             u_1            |
               |           m2*L*u_5         |
      G(t,u) = |             u_4            |
               | m2*L*sin(u_4) - (m1+m2)*g  |
               |             u_5            |
               |         -g*L*cos(u_4)      |

   Using an implicit setup for PETSc's ODE solver TS

           F(t,u,udot) = 0

   Here, we need to tell PETSc how to compute F for a given, solver
   dependent, input and possibly how to sample the 'implicit Jacobian'

           shift * dF/dudot + dF/du

   where 'shift' is a solver-dependent scalar.

   See lotka-volterra.c for a case with the explicit form and for
   more information on TS.
*/
#include <petscts.h>

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* define a type to hold our parameters */
typedef struct {
  PetscReal m1, m2, g, L;
} Baton;

/*
  This is the callback PETSc will use to sample the implicit residual. Inputs are
   - t sampling time
   - U the current state vector,
   - Udot the approximate time derivative (solver specific)
   - ctx is the application (user) context passed with TSSetIFunction
  Given these values, we need to populate F.

  For example, when using backward Euler, the nonlinear problem at time step n+1 is

       F(t_n1,U_n1,(Un1-Un)/dt) = 0

  where n1 = n+1 and dt = t_n1 - t_n.
  The user does not have to be aware of the solver-specific details,
  and it is only in charge of the input-output callback.
*/
PetscErrorCode IFunction(TS ts, PetscReal t, Vec U, Vec Udot, Vec F, void *ctx)
{
  const PetscScalar *u, *udot;
  PetscScalar       *f;
  Baton             *baton = (Baton *)ctx;

  PetscFunctionBeginUser;
  /* Tell PETSc we will only read values from U and Udot */
  PetscCall(VecGetArrayRead(U, &u));
  PetscCall(VecGetArrayRead(Udot, &udot));

  /* Tell PETSc we will write values to F */
  PetscCall(VecGetArrayWrite(F, &f));

  /* precompute */
  PetscScalar g     = baton->g;
  PetscScalar m1pm2 = baton->m1 + baton->m2;
  PetscScalar L     = baton->L;
  PetscScalar Ls4   = L * PetscSinScalar(u[4]);
  PetscScalar Lc4   = L * PetscCosScalar(u[4]);
  PetscScalar m2Ls4 = baton->m2 * Ls4;
  PetscScalar m2Lc4 = baton->m2 * Lc4;

  /* Evaluate F(t,u,u_dot) = M(u)*u_dot - G(u) */
  f[0] = udot[0] - u[1];
  f[1] = m1pm2 * udot[1] - m2Ls4 * udot[5] - u[5] * u[5] * m2Lc4;
  f[2] = udot[2] - u[3];
  f[3] = m1pm2 * udot[3] + m2Lc4 * udot[5] - u[5] * u[5] * m2Ls4 + m1pm2 * g;
  f[4] = udot[4] - u[5];
  f[5] = -Ls4 * udot[1] + Lc4 * udot[3] + L * L * udot[5] + g * Lc4;

  /* Inform PETSc we are done accessing the data */
  PetscCall(VecRestoreArrayRead(U, &u));
  PetscCall(VecRestoreArrayRead(Udot, &udot));
  PetscCall(VecRestoreArrayWrite(F, &f));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/*
  This is the callback PETSc will use to sample the Jacobian needed by the solver.
  All implicit ODE solvers are recast to use the operator

    s * dF/dUdot + dF/dU

  For this example, we need to assemble the matrix s * M(u) - dG/du

  Inputs to this function are:
   - t sampling time
   - U the current state vector,
   - Udot the approximate time derivative (solver specific)
   - 's' is a solver- and time-step-dependent scalar
   - A and P the matrices to populate.
   - ctx is the application (user) context passed with TSSetIJacobian
*/
PetscErrorCode IJacobian(TS ts, PetscReal t, Vec U, Vec Udot, PetscReal s, Mat A, Mat P, void *ctx)
{
  const PetscScalar *u, *udot;
  PetscInt           i[]   = {0, 1, 2, 3, 4, 5};
  Baton             *baton = (Baton *)ctx;
  PetscScalar        p[36] = {0};

  PetscFunctionBeginUser;
  /* Tell PETSc we will only read values from U and Udot */
  PetscCall(VecGetArrayRead(U, &u));
  PetscCall(VecGetArrayRead(Udot, &udot));

  /* precompute */
  PetscScalar g         = baton->g;
  PetscScalar m1pm2     = baton->m1 + baton->m2;
  PetscScalar L         = baton->L;
  PetscScalar Ls4       = L * PetscSinScalar(u[4]);
  PetscScalar Lc4       = L * PetscCosScalar(u[4]);
  PetscScalar dLs4du4   = L * PetscCosScalar(u[4]);
  PetscScalar dLc4du4   = -L * PetscSinScalar(u[4]);
  PetscScalar m2Ls4     = baton->m2 * Ls4;
  PetscScalar m2Lc4     = baton->m2 * Lc4;
  PetscScalar m2dLs4du4 = baton->m2 * dLs4du4;
  PetscScalar m2dLc4du4 = baton->m2 * dLc4du4;

  /* Evaluate s * dF/dUdot + dF/dU = s * M(u) - dG/dU */
  p[6 * 0 + 0] = s * 1.0;
  p[6 * 0 + 1] = -1.0;
  p[6 * 1 + 1] = s * m1pm2;
  p[6 * 1 + 4] = -u[5] * u[5] * m2dLc4du4 - m2dLs4du4 * udot[5];
  p[6 * 1 + 5] = -s * m2Ls4 - 2.0 * u[5] * m2Lc4;
  p[6 * 2 + 2] = s * 1;
  p[6 * 2 + 3] = -1.0;
  p[6 * 3 + 3] = s * m1pm2;
  p[6 * 3 + 4] = m2dLc4du4 * udot[5] - 2.0 * u[5] * m2dLs4du4;
  p[6 * 3 + 5] = s * m2Lc4 - 2.0 * u[5] * m2Ls4;
  p[6 * 4 + 4] = s * 1.0;
  p[6 * 4 + 5] = -1.0;
  p[6 * 5 + 1] = -s * Ls4;
  p[6 * 5 + 3] = s * Lc4;
  p[6 * 5 + 4] = -dLs4du4 * udot[1] + dLc4du4 * udot[3] + dLc4du4 * g;
  p[6 * 5 + 5] = s * L * L;

  PetscCall(VecRestoreArrayRead(U, &u));
  PetscCall(VecRestoreArrayRead(Udot, &udot));

  /* Insert values */
  PetscCall(MatSetValues(P, 6, i, 6, i, p, INSERT_VALUES));
  PetscCall(MatAssemblyBegin(P, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(P, MAT_FINAL_ASSEMBLY));
  if (A != P) { /* This is true when we use matrix-free or finite differencing */
    PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* Define a callback that will be called at the end of each time step.
   Monitors are supposed to provide run-time analysis of a solver.
   Here we store the time and solution histories for plotting later.
   See plot_baton.m or plot_baton.py */
#define MAX_STORE 8192
typedef struct {
  PetscScalar data[7][MAX_STORE];
} MonitorCtx;

/* Monitor callback invoked by PETSc. Inputs are
   - stepid is the current step number
   - t the current time
   - U the current solution vector
   - ctx is a user context passed with TSMonitorSet
*/
PetscErrorCode Monitor(TS ts, PetscInt stepid, PetscReal t, Vec U, void *ctx)
{
  MonitorCtx        *mctx = (MonitorCtx *)ctx;
  const PetscScalar *u;

  PetscFunctionBeginUser;
  if (stepid >= MAX_STORE) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(VecGetArrayRead(U, &u));
  mctx->data[0][stepid] = u[0];
  mctx->data[1][stepid] = u[1];
  mctx->data[2][stepid] = u[2];
  mctx->data[3][stepid] = u[3];
  mctx->data[4][stepid] = u[4];
  mctx->data[5][stepid] = u[5];
  mctx->data[6][stepid] = t;
  PetscCall(VecRestoreArrayRead(U, &u));
  PetscFunctionReturn(PETSC_SUCCESS);
}

int main(int argc, char *argv[])
{
  TS         ts;
  Mat        A;
  Vec        U;
  Baton      appCtx;
  MonitorCtx mctx;
  PetscBool  dump;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  /* Default parameters */
  appCtx.m1 = 0.1;
  appCtx.m2 = 0.1;
  appCtx.L  = 1;
  appCtx.g  = 9.81;
  PetscCall(PetscOptionsGetReal(NULL, NULL, "-m1", &appCtx.m1, NULL));
  PetscCall(PetscOptionsGetReal(NULL, NULL, "-m2", &appCtx.m2, NULL));
  PetscCall(PetscOptionsGetReal(NULL, NULL, "-L", &appCtx.L, NULL));
  PetscCall(PetscOptionsGetReal(NULL, NULL, "-g", &appCtx.g, NULL));

  /* Create vector and matrix to hold state and Jacobian */
  PetscCall(VecCreateSeq(PETSC_COMM_SELF, 6, &U));
  PetscCall(MatCreateSeqDense(PETSC_COMM_SELF, 6, 6, NULL, &A));

  /* Create ODE solver context */
  PetscCall(TSCreate(PETSC_COMM_SELF, &ts));

  /* Pass the residual evaluation routine.
     PETSc can compute the Jacobian by finite differencing,
     but we have an exact formulation at our disposable, so we use it

     Jacobian testing can be done exactly as discussed in lotka-volterra.c
  */
  PetscCall(TSSetIFunction(ts, NULL, IFunction, &appCtx));
  PetscCall(TSSetIJacobian(ts, A, A, IJacobian, &appCtx));
  PetscCall(MatDestroy(&A));

  /* Instruct PETSc to call a user-defined monitoring routine
     at each time step */
  PetscCall(TSMonitorSet(ts, Monitor, &mctx, NULL));

  /* Customize the solver (can be overridden from command line)
     Here we use a solver based on backward-differentiation formulas */
  PetscCall(TSSetType(ts, TSBDF));
  PetscCall(TSSetTimeStep(ts, 4.0 / 25));
  PetscCall(TSSetMaxTime(ts, 4.0));
  PetscCall(TSSetExactFinalTime(ts, TS_EXACTFINALTIME_INTERPOLATE));

  /* Allow command line customization */
  PetscCall(TSSetFromOptions(ts));

  /* Initial conditions (see Matlab webpage) */
  PetscCall(VecSetValue(U, 0, 0.0, INSERT_VALUES));
  PetscCall(VecSetValue(U, 1, 4.0, INSERT_VALUES));
  PetscCall(VecSetValue(U, 2, appCtx.L, INSERT_VALUES));
  PetscCall(VecSetValue(U, 3, 20.0, INSERT_VALUES));
  PetscCall(VecSetValue(U, 4, -PETSC_PI / 2.0, INSERT_VALUES));
  PetscCall(VecSetValue(U, 5, 2.0, INSERT_VALUES));

  /* Solve the ODE */
  PetscCall(TSSolve(ts, U));

  /* Dump data for visualization if requested via command line
     
     MATLAB:
         ./baton -view_data :baton.m:ascii_matlab
         Use plot_baton.m to visualize the solution
     Python:
         ./baton -view_data binary:baton.dat:
         Use plot_baton.py to visualize the solution
  */
  PetscCall(PetscOptionsHasName(NULL, NULL, "-view_data", &dump));
  if (dump) {
    PetscInt nsteps;
    Mat      D;

    PetscCall(TSGetStepNumber(ts, &nsteps));
    PetscCall(MatCreateSeqDense(PETSC_COMM_SELF, PetscMin(nsteps, MAX_STORE), 7, (PetscScalar *)mctx.data, &D));
    PetscCall(MatDenseSetLDA(D, MAX_STORE));
    PetscCall(PetscObjectSetName((PetscObject)D, "q"));
    PetscCall(MatViewFromOptions(D, NULL, "-view_data"));
    PetscCall(MatDestroy(&D));
  }

  /* Cleanup */
  PetscCall(TSDestroy(&ts));
  PetscCall(VecDestroy(&U));
  PetscCall(PetscFinalize());
  return 0;
}
