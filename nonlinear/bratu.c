/*
   Solve the Bratu equations on a regular grid

     - u_xx - u_yy - lamba * exp(u) = 0 in [0,1]^2
     u = 0 on the boundary

   This is a parallel implementation using PETSc's regular grid object (DMDA).
   Boundary points are represented. See ../poisson/dmda.c for more details.

   In this example we show how to setup a nonlinear solver (SNES).

   SNES solves the root-finding problem F(U) = b, where F is nonlinear and b a given right-hand side (usually zero).
   We need to pass PETSc the callbacks to sample F at a given state and, possibly, evaluate the Jacobian dF/dU
   However, since we are using a grid-aware code with DMDA, we can efficiently sample the Jacobian via colored finite
   differences too and check our routine.

   Solution can be exported for visualization to Paraview using, e.g.:

      $ ./bratu -da_grid_x 32 -da_grid_y 32 -snes_view_solution vtk:sol.vts:

*/

/* Include nonlinear solver API (SNES) and its dependencies */
#include <petscsnes.h>
#include <petscdmda.h>

/* Define a type to hold our parameters */
typedef struct {
  PetscReal lambda;
} Bratu;

/* This is the callback to sample the nonlinear residual
   - snes: the nonlinear solver context
   - U: the current state vector
   - F: the output of this function, holding F(U)
   - ctx: a user-defined application context passed via SNESSetFunction
*/
static PetscErrorCode Residual(SNES snes, Vec U, Vec F, void *ctx)
{
  DM                  dm;
  const PetscScalar **u;
  PetscScalar       **f;
  PetscScalar         hx, hy, hxm2, hym2;
  PetscInt            xs, ys, xm, ym, mx, my;
  Bratu              *bratu = (Bratu *)ctx; /* access problem parameters from the user-defined context */

  PetscFunctionBeginUser;
  /* we can access the DM from the SNES if we attached it using SNESSetDM() */
  PetscCall(SNESGetDM(snes, &dm));
  PetscCall(DMDAGetCorners(dm, &xs, &ys, NULL, &xm, &ym, NULL));
  PetscCall(DMDAGetInfo(dm, 0, &mx, &my, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
  hx   = 1.0 / (PetscReal)(mx - 1);
  hy   = 1.0 / (PetscReal)(my - 1);
  hxm2 = 1. / (hx * hx);
  hym2 = 1. / (hy * hy);

  /* Before accessing values of U we need to update ghost locations for
     stencil evaluations. The DM API makes it easy to perform such operation.
     First, we retrieve a scratch working LOCAL vector from the DM that holds
     values outside the locally owned portion of the domain to allow performing
     stencil evaluations. We can retrieve as many vectors as we want, as long as we
     restore them back when we are done.
     Persistent vectors are also available using DMGetNamedLocalVector
  */
  Vec lU;
  PetscCall(DMGetLocalVector(dm, &lU));
  PetscCall(DMGlobalToLocalBegin(dm, U, INSERT_VALUES, lU));
  PetscCall(DMGlobalToLocalEnd(dm, U, INSERT_VALUES, lU));
  PetscCall(DMDAVecGetArrayRead(dm, lU, &u));
  PetscCall(DMDAVecGetArrayWrite(dm, F, &f));

  for (PetscInt j = ys; j < ys + ym; ++j)
    for (PetscInt i = xs; i < xs + xm; ++i) /* Residual on the domain boundary */
      if (i == 0 || j == 0 || i == mx - 1 || j == my - 1) f[j][i] = u[j][i];
      else { /* Residual on interior points */
        /* Get values of state at stencil locations

             n
             |
          w--x--e
             |
             s

        */
        PetscScalar ux = u[j][i];
        PetscScalar uw = u[j][i - 1];
        PetscScalar ue = u[j][i + 1];
        PetscScalar us = u[j - 1][i];
        PetscScalar un = u[j + 1][i];

        /* Enforce boundary conditions at neighboring points.
           Setting these values causes the Jacobian to be symmetric.
           This is a well-known trick when solving discretized PDEs with essential boundary conditions */
        if (i - 1 == 0) uw = 0.0;
        if (i + 1 == mx - 1) ue = 0.0;
        if (j - 1 == 0) us = 0.0;
        if (j + 1 == my - 1) un = 0.0;

        /* Evaluate the residual at grid point */
        PetscScalar uxx = (2.0 * ux - uw - ue) * hxm2;
        PetscScalar uyy = (2.0 * ux - un - us) * hym2;
        f[j][i]         = uxx + uyy - bratu->lambda * PetscExpScalar(ux);
      }

  PetscCall(DMDAVecRestoreArrayRead(dm, lU, &u));
  PetscCall(DMDAVecRestoreArrayWrite(dm, F, &f));

  /* lU is a scratch vector. Tell DM we are done with it */
  PetscCall(DMRestoreLocalVector(dm, &lU));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* This is the callback to sample the Jacobian of the nonlinear residual
   - snes: the nonlinear solver context
   - U: the current state vector
   - A, P: the output of this function
       A is the matrix used to compute the action of the Jacobian
       P is the matrix used to construct the preconditioner
   - ctx: a user-defined application context passed via SNESSetJacobian

   Users are in charge of 'assembling' those matrices the way they like.

   Here we use a standard convention in PETSc and we only populate the
   'P' matrix. This is because in case we select Jacobian-free Krylov solvers
   using the -snes_mf_operator option,'A' will be automatically handled
   internally by SNES as a MATMFFD (matrix-free, finite-difference) type,
   and we will be only in charge of producing a (possibly approximate) Jacobian
   that will be used to construct the preconditioner.

   At a given linearization point uk, the action of the MFFD Jacobian on a
   vector v is computed as:

         F'(uk)*v = [F(uk+h*v) - F(uk)]/h

   where the perturbation h is a scalar internally computed by PETSc.

   A note on Jacobian-free in PETSc:
      - snes_mf_operator uses MFFD A and leave P to the user
      - snes_mf uses MFFD for A and changes the Jacobian callback
*/
static PetscErrorCode Jacobian(SNES snes, Vec U, Mat A, Mat P, void *ctx)
{
  DM                  dm;
  const PetscScalar **u;
  PetscScalar         hx, hy, hxm2, hym2;
  PetscInt            xs, ys, xm, ym, mx, my;
  Bratu              *bratu = (Bratu *)ctx;

  PetscFunctionBeginUser;
  PetscCall(SNESGetDM(snes, &dm));
  PetscCall(DMDAGetCorners(dm, &xs, &ys, NULL, &xm, &ym, NULL));
  PetscCall(DMDAGetInfo(dm, 0, &mx, &my, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
  hx   = 1.0 / (PetscReal)(mx - 1);
  hy   = 1.0 / (PetscReal)(my - 1);
  hxm2 = 1. / (hx * hx);
  hym2 = 1. / (hy * hy);

  /* We don't need ghosted values for U, since the only nonlinearity we
     have is independent on the partial differential operator.
     We can thus access U values directly */
  PetscCall(DMDAVecGetArrayRead(dm, U, &u));

  /*
     Compute entries for the locally owned part of the matrix.
     Here, we set all entries for a particular row at once using MatSetValuesStencil,
     which internally remaps stencil indices to global matrix indices.
  */
  PetscCall(MatZeroEntries(P));
  for (PetscInt j = ys; j < ys + ym; j++) {
    for (PetscInt i = xs; i < xs + xm; i++) {
      MatStencil  col[5], row;
      PetscScalar v[5];

      row.j = j;
      row.i = i;
      if (i == 0 || j == 0 || i == mx - 1 || j == my - 1) { /* boundary points */
        v[0] = 1.0;
        PetscCall(MatSetValuesStencil(P, 1, &row, 1, &row, v, INSERT_VALUES));
      } else { /* interior grid points */
        /* With the residual, we got values at the stencil locations.
           When computing the Jacobian instead, we need to get their derivatives */
        PetscScalar duw = 1.0;
        PetscScalar due = 1.0;
        PetscScalar dun = 1.0;
        PetscScalar dus = 1.0;
        if (i - 1 == 0) duw = 0.0;
        if (i + 1 == mx - 1) due = 0.0;
        if (j - 1 == 0) dus = 0.0;
        if (j + 1 == my - 1) dun = 0.0;

        /* assemble stencil contribution
           recall residual at grid point:

           PetscScalar uxx = (2.0 * ux - uw - ue) * hxm2;
           PetscScalar uyy = (2.0 * ux - un - us) * hym2;
           f[j][i] = uxx + uyy - lambda * PetscExpScalar(ux);
        */

        /* df[j][i] / dux */
        v[0]     = 2.0 * (hxm2 + hym2) - bratu->lambda * PetscExpScalar(u[j][i]);
        col[0].j = j;
        col[0].i = i;

        /* df[j][i] / duw */
        v[1]     = -hxm2 * duw;
        col[1].j = j;
        col[1].i = i - 1;

        /* df[j][i] / due */
        v[2]     = -hxm2 * due;
        col[2].j = j;
        col[2].i = i + 1;

        /* df[j][i] / dus */
        v[3]     = -hym2 * dus;
        col[3].j = j - 1;
        col[3].i = i;

        /* df[j][i] / dun */
        v[4]     = -hym2 * dun;
        col[4].j = j + 1;
        col[4].i = i;

        PetscCall(MatSetValuesStencil(P, 1, &row, 5, col, v, INSERT_VALUES));
      }
    }
  }

  /* Assemble matrices (see discussion above) */
  PetscCall(MatAssemblyBegin(P, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(P, MAT_FINAL_ASSEMBLY));
  if (A != P) {
    /* We need to tell the MFFD approximation to reset the linearization point
       this is why we call MatAssemblyBegin/End on the matrix object. */
    PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

int main(int argc, char **argv)
{
  DM    dm;
  SNES  snes;
  KSP   ksp;
  PC    pc;
  Vec   x;
  Bratu appCtx;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  /* Default parameters */
  appCtx.lambda = 6.8;
  PetscCall(PetscOptionsGetReal(NULL, NULL, "-lambda", &appCtx.lambda, NULL));

  /* Create distributed array (DMDA) to manage parallel grid and vectors */
  PetscCall(DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_STAR, 7, 7, PETSC_DECIDE, PETSC_DECIDE, 1, 1, NULL, NULL, &dm));
  PetscCall(DMSetFromOptions(dm));
  PetscCall(DMSetUp(dm));
  PetscCall(DMDASetUniformCoordinates(dm, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0));
  PetscCall(DMViewFromOptions(dm, NULL, "-view_dm"));

  /* Create global vector from the DM */
  PetscCall(DMCreateGlobalVector(dm, &x));

  /* Give the vector a name. This is usually used in visualization routines.
     Note that we can name any PetscObject, and we need to downcast 'x' to it */
  PetscCall(PetscObjectSetName((PetscObject)x, "solution"));

  /* Create nonlinear solver context
     We use Newton with backtracking line-search as default.
     Again, these choices can be overridden at command line.
     Run with '-help' and grep for 'snes_' */
  PetscCall(SNESCreate(PETSC_COMM_WORLD, &snes));
  PetscCall(SNESSetType(snes, SNESNEWTONLS));

  /* Inform SNES about the DM */
  PetscCall(SNESSetDM(snes, dm));

  /* Inform SNES how to sample the nonlinear residual */
  PetscCall(SNESSetFunction(snes, NULL, Residual, &appCtx));

  /* Inform SNES how to sample the Jacobian of the nonlinear residual
     Here we pass NULL as placeholders for A and P matrices,
     since we have informed SNES about the DM, which knows how
     to produce matrices with the proper sparisity pattern setup.
     In case a DM is not used, matrices have to be created upfront
     and passed to this routine. See for example the TSSetRHSJacobian
     call in lotka-volterra.c */
  PetscCall(SNESSetJacobian(snes, NULL, NULL, Jacobian, &appCtx));

  /* We can access lower level solvers KSP and PC.
     Here we set our default PC to PCGAMG. */
  PetscCall(SNESGetKSP(snes, &ksp));
  PetscCall(KSPGetPC(ksp, &pc));
  PetscCall(PCSetType(pc, PCGAMG));

  /* Allow command-line customization

     We can use geometric multigrid via command line options, for example:

        -da_grid_x 9 -da_grid_y 9 -da_refine 6 -snes_monitor_short -pc_type mg -pc_mg_levels 5 -pc_mg_galerkin

     The options above will solve a problem with 512 cells per dimension (513 grid points) with 5 levels of multigrid.

     If you want to test the correctness of the Jacobian routine, run with -snes_test_jacobian -snes_max_it 1.
     You can use -snes_test_jacobian_view for more verbose output, and -snes_test_jacobian <threshold> to
     impose a thresholding on the values to be checked.

     If we wouldn't have a Jacobian routine available, SNES offers the possibility of
     computing the Jacobian by finite differencing the residual function via '-snes_fd'.
     Here, since we are using DMDA, we can also use '-snes_fd_color' which is a sparsity-aware, more efficient
     construction of the Jacobian.
  */
  PetscCall(SNESSetFromOptions(snes));

  /* Solve the nonlinear equations */
  PetscCall(SNESSolve(snes, NULL, x));

  /* Destroy PETSc objects that are no longer needed */
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&x));

  /* Finalize */
  PetscCall(PetscFinalize());
  return 0;
}
