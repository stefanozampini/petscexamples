# This is an example of how to write a TS solver in Python
# If you are interested in more details on TSPTYHON,
# you can find them in PETSc's source code at
# src/binding/petsc4py/src/petsc4py/PETSc/libpetsc4py.pyx
# and look for TSCreate_Python

# Here we wrap odeint from scipy.integrate
from scipy.integrate import odeint


# our class must inherit from object
class tsodeint:
    def __init__(self):
        pass

    # This will be called during TSSetUp -> TSSetUp_Python
    def setUp(self, ts):
        self.y = ts.getSolution().duplicate()
        self.f = ts.getSolution().duplicate()

    # This will be called during TSStep -> TSStep_Python -> TSStep_Python_default
    def solveStep(self, ts, tpdt, v):
        # Access current solution as np.array
        y0 = ts.getSolution().getArray()

        # Access time
        t0 = ts.getTime()

        # Wrap odeint input to call user provided RHS function
        def _func(y, t):
            self.y.getArray()[:] = y[:]
            ts.computeRHSFunction(t, self.y, self.f)
            return self.f.getArray()

        # Integrate from t0 to t0 + dt
        sol = odeint(_func, y0, [t0, tpdt])

        # Return solution to PETSc
        v.getArray()[:] = sol[-1][:]
