#
# This example shows how to solve a image denoising problem with a TAO solver
#
# Given a noisy image Y, we want to solve
#
#    argmin 1/2 * || X - Y ||^ 2 + lambda * TV(X,beta)
#       X
#
# where TV(X,beta) = sqrt(|| grad(X) || ^ 2 + beta)
#
# Here we assume the image is a 2-D scalar (for gray-scale) or vector (for RGB etc.) function defined on [0,1]^2
#
# For details, see Blomgren, Peter, and Tony F. Chan.
# "Color TV: total variation methods for restoration of vector-valued images."
# IEEE transactions on image processing 7.3 (1998): 304-309.
#

# Initialize PETSc and import the module
import sys
import petsc4py

petsc4py.init(sys.argv)
from petsc4py import PETSc

# We use numpy, scipy and matplotlib
import numpy as np
from scipy import ndimage
from matplotlib import image
from matplotlib import pyplot as plt

# Access options database
OptDB = PETSc.Options()

# Get command line options
beta = OptDB.getReal('beta', 1.0e-6)
lam = OptDB.getReal('lambda', 1.0e-8)
filename = OptDB.getString('filename', 'noisylena.png')

# Read image
image = image.imread(filename)
hx = 1 / image.shape[0]
hy = 1 / image.shape[1]


# Total variation objective
def TV(u):
    dudx = ndimage.sobel(u, axis=0, mode='constant') / hx
    dudy = ndimage.sobel(u, axis=1, mode='constant') / hy
    return np.sqrt(hx * hy * np.hypot(dudx, dudy) ** 2 + beta)


# Gradient of the total variation
def gradTV(u):
    dudx = ndimage.sobel(u, axis=0, mode='constant') / hx
    dudy = ndimage.sobel(u, axis=1, mode='constant') / hy
    n = np.sqrt(hx * hy * np.hypot(dudx, dudy) ** 2 + beta)

    d2udx2 = ndimage.sobel(dudx / n, axis=0, mode='constant') / hx
    d2udy2 = ndimage.sobel(dudy / n, axis=1, mode='constant') / hy
    return -hx * hy * (d2udx2 + d2udy2)


# Callbacks for TAO solvers


# This is the callback for the objective function we want to minimize
def myObjective(_tao, x):
    x = x.getArray(readonly=True).reshape(image.shape)
    return 0.5 * hx * hy * np.linalg.norm(x - image) ** 2 + lam * np.sum(TV(x))


# This is the callback for the gradient of the objective function we want to minimize
def myGradient(_tao, x, g):
    x = x.getArray(readonly=True).reshape(image.shape)
    g = g.getArray().reshape(image.shape)

    g[:] = hx * hy * (x - image) + lam * gradTV(x)


# This is the callback for monitoring
def myMonitor(tao):
    it = tao.getIterationNumber()
    x = tao.getSolution().getArray(readonly=True).reshape(image.shape)
    PETSc.Sys.Print(
        f'  myMonitor {it}: {0.5 * hx * hy * np.linalg.norm(x - image) ** 2} {lam * np.sum(TV(x))}',
        comm=tao.getComm(),
    )


# Create PETSc vector to hold an image
x = PETSc.Vec().createSeq(size=image.size)
x.getArray()[:] = image.reshape(-1)

# Create TAO solver and set callbacks
# We use a BFGS solver
tao = PETSc.TAO().create(comm=PETSc.COMM_SELF)
tao.setType(PETSc.TAO.Type.BLMVM)
tao.setObjective(myObjective)
tao.setGradient(myGradient, x.duplicate())
tao.setSolution(x)
tao.setMonitor(myMonitor)
tao.setMaximumIterations(100)

# Set variable lower and upper bounds
xl = x.duplicate()
xu = x.duplicate()
xl.set(0.0)
xu.set(1.0)
tao.setVariableBounds((xl, xu))

# Allow command line customization
tao.setFromOptions()

# Solve the minimization problem
tao.solve()

# Plot the solution
s = tao.getSolution().getArray().reshape(image.shape)
fig = plt.figure()
fig.add_subplot(1, 2, 1, title='original').imshow(image)
fig.add_subplot(1, 2, 2, title='denoised').imshow(s)
plt.show()
