# Python version of plot_baton.m
# Data must have been generated with
# ./baton -view_data binary:baton.dat:

from petsc4py import PETSc
import numpy as np
import matplotlib.pyplot as plt

viewer = PETSc.Viewer().createBinary('baton.dat', 'r')
Q = PETSc.Mat().create()
Q.setType('dense')
Q.load(viewer)
dQ = Q.getDenseArray()

plt.figure('Motion of a Thrown Baton')
L = 1
for q in dQ:
    theta = q[4]
    X = q[0]
    Y = q[2]
    xvals = np.array([X, X + L * np.cos(theta)])
    yvals = np.array([Y, Y + L * np.sin(theta)])
    plt.plot(xvals, yvals, 'b')
    plt.plot(xvals[0], yvals[0], 'ro')
    plt.plot(xvals[1], yvals[1], 'go')
plt.xlim([0, 22])
plt.ylim([0, 25])
plt.show()
