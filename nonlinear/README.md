This folder contains examples on how to use PETSc nonlinear solvers:

- [bratu](bratu.c): Solves the Bratu equations on a regular grid using PETSc's nonlinear solver SNES. This is a partial differential equations example. Take a look at this [code](../poisson/dmda.c) for additional information on the discretization.

- [lotka-volterra](lotka-volterra.c): Solves the system of ordinary differential equations (ODE) describing the dynamics of a two populations model using PETSc's ODE solver TS. Python version [here](lotka-volterra.py).

- [baton](baton.c): Solves the equations of motion for a baton thrown into air using an implicit ODE solver with TS. See [here](https://www.mathworks.com/help/matlab/math/solve-equations-of-motion-for-baton.html) for more details.

- [rosenbrock](rosenbrock.c): Solves an unconstrained minimization problem using PETSc's optimization solver TAO.

- [denoising](denoising.py): a Python driver to solve a minimization problem with Total-Variation regularization using TAO.

It also contains additional Python code to be used as solvers from PETSc:

- [myts](myts.py): A very basic wrapper for scipy.integrate.odeint showcasing TSPYTHON.

