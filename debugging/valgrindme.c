/*

  This example contains errors and memory leaks to help you familiarize with valgrind.

  If run without valgrind, the code will produce some garbage like this and run to completion

$ ./valgrindme
i1: 1852141647 1864388685 1869182064 -16747666 -253701943 0
i2: 2 2146435072 2 2146435072 2 2146435072
i3: 0 0 0 0 0 0

  With valgrind, we can get meaningful error reports to ease debugging, for example the first error we get is

$ valgrind ./valgrindme
==3250984== Invalid read of size 4
==3250984==    at 0x40153C: main (valgrindme.c:102)
==3250984==  Address 0xb16afb4 is 0 bytes after a block of size 1,636 alloc'd
==3250984==    at 0x4845803: memalign (/builddir/build/BUILD/valgrind-3.18.1/coregrind/m_replacemalloc/vg_replace_malloc.c:1516)
==3250984==    by 0x484591F: posix_memalign (/builddir/build/BUILD/valgrind-3.18.1/coregrind/m_replacemalloc/vg_replace_malloc.c:1688)
==3250984==    by 0x49D52F9: PetscMallocAlign ($PETSC_DIR/src/sys/memory/mal.c:54)
==3250984==    by 0x49D9F51: PetscTrMallocDefault ($PETSC_DIR/src/sys/memory/mtr.c:186)
==3250984==    by 0x4012E2: main (valgrindme.c:93)
...

This tells us that in main at line 102 we are trying to read 4 bytes from a location we are not allowed to access.
And that the faulty location is 0 bytes after some 1636 bytes allocated in main, line 93.
Inspecting the code, the 1636 number seems strange (expecting 6 right?). This is because PETSc in debug modes wraps heap
allocated memory with some error-checking structures to allow catching some of the most common memory bugs.
This feature can be disabled by using -malloc_debug 0.

$ valgrind ./valgrindme -malloc_debug 0
==3251669== Invalid read of size 4
==3251669==    at 0x40153C: main (valgrindme.c:102)
==3251669==  Address 0xb163e24 is 14 bytes after a block of size 6 alloc'd
==3251669==    at 0x4845803: memalign (/builddir/build/BUILD/valgrind-3.18.1/coregrind/m_replacemalloc/vg_replace_malloc.c:1516)
==3251669==    by 0x484591F: posix_memalign (/builddir/build/BUILD/valgrind-3.18.1/coregrind/m_replacemalloc/vg_replace_malloc.c:1688)
==3251669==    by 0x49D52F9: PetscMallocAlign ($PETSC_DIR/src/sys/memory/mal.c:54)
==3251669==    by 0x4012E2: main (valgrindme.c:93)
...


To catch memory leaks in PETSc programs, run with -malloc_dump and you should get something like the output below,
reporting the lines of code where the allocation took place.

$ ./valgrindme -malloc_dump
...
[ 0] 32 bytes main() at valgrindme.c:99
      [0]  PetscMallocA() at $PETSC_DIR/src/sys/memory/mal.c:66
[ 0] 32 bytes main() at valgrindme.c:96
      [0]  PetscMallocA() at $PETSC_DIR/src/sys/memory/mal.c:63
[ 0] 16 bytes main() at valgrindme.c:93

You can also use valgrind, that has a more advanced and precise leak detection system.
The leak check option is not part of the rc file.
This is because the leaking report will be huge if your program crashes for some reason.
Good practice suggest to checks for leaks only when you know your code is clean.

$ valgrind --leak-check=full ./valgrindme -malloc_debug 0
...
==3253221== 6 bytes in 1 blocks are definitely lost in loss record 1 of 3
==3253221==    at 0x4845803: memalign (/builddir/build/BUILD/valgrind-3.18.1/coregrind/m_replacemalloc/vg_replace_malloc.c:1516)
==3253221==    by 0x484591F: posix_memalign (/builddir/build/BUILD/valgrind-3.18.1/coregrind/m_replacemalloc/vg_replace_malloc.c:1688)
==3253221==    by 0x49D52F9: PetscMallocAlign ($PETSC_DIR/src/sys/memory/mal.c:54)
==3253221==    by 0x4012E2: main (valgrindme.c:93)
==3253221==
==3253221== 24 bytes in 1 blocks are definitely lost in loss record 2 of 3
==3253221==    at 0x4845803: memalign (/builddir/build/BUILD/valgrind-3.18.1/coregrind/m_replacemalloc/vg_replace_malloc.c:1516)
==3253221==    by 0x484591F: posix_memalign (/builddir/build/BUILD/valgrind-3.18.1/coregrind/m_replacemalloc/vg_replace_malloc.c:1688)
==3253221==    by 0x49D52F9: PetscMallocAlign ($PETSC_DIR/src/sys/memory/mal.c:54)
==3253221==    by 0x49D7B02: PetscMallocA ($PETSC_DIR/src/sys/memory/mal.c:420)
==3253221==    by 0x4013D9: main (valgrindme.c:96)
==3253221==
==3253221== 24 bytes in 1 blocks are definitely lost in loss record 3 of 3
==3253221==    at 0x4845803: memalign (/builddir/build/BUILD/valgrind-3.18.1/coregrind/m_replacemalloc/vg_replace_malloc.c:1516)
==3253221==    by 0x484591F: posix_memalign (/builddir/build/BUILD/valgrind-3.18.1/coregrind/m_replacemalloc/vg_replace_malloc.c:1688)
==3253221==    by 0x49D52F9: PetscMallocAlign ($PETSC_DIR/src/sys/memory/mal.c:54)
==3253221==    by 0x49D7B02: PetscMallocA ($PETSC_DIR/src/sys/memory/mal.c:420)
==3253221==    by 0x4014D4: main (valgrindme.c:99)

*/

#include <petsc.h>

#define dumpval(A) printf("%s: %d %d %d %d %d %d\n", PetscStringize(A), (A)[0], (A)[1], (A)[2], (A)[3], (A)[4], (A)[5]);

int main(int argc, char *argv[])
{
  int *i1, *i2, *i3;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));
  /* This is wrong, allocates 6 bytes only.
     PetscMalloc is just a convenient wrapper around malloc */
  PetscCall(PetscMalloc(6, &i1));

  /* This is correct, allocates space for 6 ints */
  PetscCall(PetscMalloc1(6, &i2));

  /* This is correct, allocates space for 6 ints and initializes values to zero */
  PetscCall(PetscCalloc1(6, &i3));

  /* This will print garbage or segfault */
  dumpval(i1);
  /* This will print garbage since the ints are uninitialized */
  dumpval(i2);
  /* This will print all zeros */
  dumpval(i3);

  /* Free memory. These are commented out to show leaks */
  /* PetscCall(PetscFree(i3)); */
  /* PetscCall(PetscFree(i2)); */
  /* PetscCall(PetscFree(i1)); */

  PetscCall(PetscFinalize());
  return 0;
}
