/*
   Concepts:
     Debugging

   This code contains a very simple bug but it will always run to completion with no apparent errors.
   This does not mean the code is correct. Always use a debug version and check your code under valgrind while developing.

   Try commenting out the VecDestroy call and recompile.
   - When run in debug mode, this code should produce a similar stack trace to the one shown below.
   - When run without debugging, the code will still run to completion with no apparent errors.


$ ./vecerror
Vec Object: 1 MPI process
  type: seq
0.
1.
2.
3.
[0]PETSC ERROR: PetscTrFreeDefault() called from VecDestroy_Seq() at $PETSC_DIR/src/vec/vec/impls/seq/bvec2.c:753
[0]PETSC ERROR: Block [id=87(32)] at address 0x19ac420 is corrupted (probably write past end of array)
[0]PETSC ERROR: Block allocated in VecCreate_Seq() at $PETSC_DIR/src/vec/vec/impls/seq/bvec3.c:34
[0]PETSC ERROR: --------------------- Error Message --------------------------------------------------------------
[0]PETSC ERROR: Memory corruption: https://petsc.org/release/faq/#valgrind
[0]PETSC ERROR: Corrupted memory
[0]PETSC ERROR: See https://petsc.org/release/faq/ for trouble shooting.
[0]PETSC ERROR: Petsc Release Version 3.18.1, unknown
[0]PETSC ERROR: ./vecerror on a ....
[0]PETSC ERROR: Configure options ....
[0]PETSC ERROR: #1 PetscTrFreeDefault() at $PETSC_DIR/src/sys/memory/mtr.c:...
[0]PETSC ERROR: #2 VecDestroy_Seq() at $PETSC_DIR/src/vec/vec/impls/seq/bvec2.c:...
[0]PETSC ERROR: #3 VecDestroy() at $PETSC_DIR/src/vec/vec/interface/vector.c:..
[0]PETSC ERROR: #4 main() at vecerror.c:68
[0]PETSC ERROR: Reached the main program with an out-of-range error code 1. This should never happen
[0]PETSC ERROR: PETSc Option Table entries:
[0]PETSC ERROR: ...
[0]PETSC ERROR: ----------------End of Error Message -------send entire error message to petsc-maint@mcs.anl.gov----------

*/
#include <petsc.h>

int main(int argc, char *argv[])
{
  Vec          v;
  PetscScalar *x;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  /* Create a vector with 4 elements per process */
  PetscCall(VecCreate(PETSC_COMM_WORLD, &v));
  PetscCall(VecSetType(v, VECSTANDARD));
  PetscCall(VecSetSizes(v, 4, PETSC_DECIDE));

  /* Set 5 values locally: this is clearly wrong */
  PetscCall(VecGetArray(v, &x));
  for (int i = 0; i < 5; i++) x[i] = i;
  PetscCall(VecRestoreArray(v, &x));

  /* View the results: apparently no errors
     Printing stuff can be a quick-and-dirty way to uncover
     bugs, but it does not work in general and may change the
     behaviour of your code when using optimization flags */
  PetscCall(VecView(v, NULL));

  /* Destroy the vector. The call is commented out to let the program
     run to completion. Remove the comments and recompile. Now PETSc
     will complain. */
  //PetscCall(VecDestroy(&v));

  /* Finalize */
  PetscCall(PetscFinalize());
  return 0;
}
