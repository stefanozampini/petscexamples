This folder contains examples to help you familiarize with code debugging.
These examples need [valgrind](https://valgrind.org/), which is one of the most powerful tools to catch errors in your code.
This folder contains an example of [rcfile](.valgrindrc) for valgrind.

- [valgrindme.c](valgrindme.c): Help you familiarize with valgrind
- [vecerror.c](vecerror.c): A PETSc program with a simple bug that shows why using valgrind is important


