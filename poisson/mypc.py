# Writing a simple Python class like the one here
# is the Python equivalent of PCSHELL in C/C++ codes
# Here we mimic mypc.c and implement the same method.
#
# If you are interested in more details on PCPYTHON,
# you can find them in PETSc's source code at
# src/binding/petsc4py/src/petsc4py/PETSc/libpetsc4py.pyx
# and look for PCCreate_Python


# our class must inherit from object
class mypc:
    def __init__(self):
        pass

    # This will be called during PCSetUp -> PCSetUp_Python
    def setUp(self, pc):
        _, P = pc.getOperators()
        self.D = P.getDiagonal()

    # This will be called during PCApply -> PCApply_Python
    def apply(self, _pc, x, y):
        y.pointwiseDivide(x, self.D)
