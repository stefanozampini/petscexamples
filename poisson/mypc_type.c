#include "mypc.h"

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* This code shows how to setup an user-defined type of preconditioner.
   There are two functions.

   - PCCreate_User: this is the entry point specified in PCRegister(),
     and the one that will be called when PCSetType() is called.

     In this function we need to allocate the needed data structures and set the
     operation pointers, including the customized ones.
     For being able to do that, we need to include the header
     files that describe how a PC is implemented in PETSc. As all PETSc objects,
     it is something like

     {
       A_common_header_with_ops(struct _PCOps); // holds the function pointers for the type-specific implementations
       ...                                      // common stuff for all preconditioners
       void* data;                              // holds the data structures needed by the specific implementation
     }

     After we do that, PETSc will call our own implementations.

   - PCUserGetContext_User: this function is 'composed' with the object, and gives
     the possibility of providing a specific implementation of a public API.
     We use it here since we reuse the same code for a PCSHELL and this full-fledged type,
     and we need to tell PETSc how to access the private data.

   Take a look at ../sys/object.c for additional details about object oriented PETSc's design
*/

#include <petsc/private/pcimpl.h>

/* Define a type to hold the preconditioner data
   We could have stashed this definition in mypc.h, but I prefer
   not to have private details leaking to public headers */
typedef struct {
  Vec D;
} MyPC;

/* This function customizes the access to the data */
static PetscErrorCode PCUserGetContext_User(PC pc, void *ctx)
{
  PetscFunctionBeginUser;
  *(void **)ctx = pc->data;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* This function populates the PC struct with function pointers and data structures */
PetscErrorCode PCCreate_User(PC pc)
{
  MyPC *ctx;

  PetscFunctionBeginUser;
  /* allocate data structure and populate private data */
  PetscCall(PetscNew(&ctx));
  pc->data = ctx;

  /* set function pointers
     the implementantion of this functions is in mypc.c */
  pc->ops->setup   = PCSetUp_User;
  pc->ops->destroy = PCDestroy_User;
  pc->ops->apply   = PCApply_User;

  /* Public API special customization */
  PetscCall(PetscObjectComposeFunction((PetscObject)pc, "PCUserGetContext_C", PCUserGetContext_User));
  PetscFunctionReturn(PETSC_SUCCESS);
}
