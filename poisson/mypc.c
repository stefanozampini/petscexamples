#include "mypc.h"

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* This is just an example of preconditioner.
   In this case we implement the Jacobi method, where the
   preconditioner action consists in apply the inverse of the diagonal
   of the matrix

   We setup the preconditioner during PCSetUp_User, apply it to a vector
   in PCApply_User and destroy the owned data in PCDestroy_User.
*/

/* Define a type to hold the preconditioner data */
typedef struct {
  Vec D;
} MyPC;

/* Since this same code will be used for two different purposes in this set
   of examples (as a PCSHELL or as a full fledged type), we first define a function
   that allows us to access the private data structures used. This is usually not needed,
   but it gives an example of how a PETSc type can be customized for future user needs and what
   to do in a public API call like this, including raising a proper error.
   See also ../sys/object.c for additional details on PETSc classes and types.
   See at the end of the file for its implementation */
PetscErrorCode PCUserGetContext(PC, void *);

/* This function is called every time the operator is modified */
PetscErrorCode PCSetUp_User(PC pc)
{
  Mat   P;
  MyPC *ctx;

  PetscFunctionBeginUser;
  PetscCall(PCGetOperators(pc, NULL, &P));
  PetscCall(PCUserGetContext(pc, &ctx));
  /* This is the reason we raise an error in PCUserGetContext in case the ctx is NULL.
     We don't want to segfault, but instead produce a nice error message */
  if (!ctx->D) PetscCall(MatCreateVecs(P, &ctx->D, NULL));
  PetscCall(MatGetDiagonal(P, ctx->D));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* This function is called whenever the application of the preconditioner
   is needed. x is the input vector, y the output.
   In this case: y = D^-1 x */
PetscErrorCode PCApply_User(PC pc, Vec x, Vec y)
{
  MyPC *ctx;

  PetscFunctionBeginUser;
  PetscCall(PCUserGetContext(pc, &ctx));
  PetscCall(VecPointwiseDivide(y, x, ctx->D));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* This function is called when the preconditioner is destroyed.
   We are in charge of freeing the memory owned by the preconditioner */
PetscErrorCode PCDestroy_User(PC pc)
{
  MyPC *ctx;

  PetscFunctionBeginUser;
  PetscCall(PCUserGetContext(pc, &ctx));
  if (ctx) {
    PetscCall(VecDestroy(&ctx->D));
    PetscCall(PetscFree(ctx));
  }
  /* We need to remove any userdefined function composition
     since the pc can be reused as a different type. */
  PetscCall(PetscObjectComposeFunction((PetscObject)pc, "PCUserGetContext_C", NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* Access PC private data, access PCSHELL context as fallback */
PetscErrorCode PCUserGetContext(PC pc, void *ctx)
{
  PetscErrorCode (*f)(PC, void *);

  PetscFunctionBeginUser;
  /* First set to NULL the output */
  *(void **)ctx = NULL;

  /* Check if users have provided a custom implementation of the method
     We use the QueryFunction API that looks into the map of composed functions,
     using PCUserGetContext_C as key */
  PetscCall(PetscObjectQueryFunction((PetscObject)pc, "PCUserGetContext_C", &f));
  if (f) { /* someone knows what is doing, let's call the method */
    void *uctx;

    PetscCall((*f)(pc, &uctx));
    *(void **)ctx = uctx;
  } else { /* we don't know how to access a context, let's see if it is a shell */
    PetscBool isshell;

    /* Differently from setters, getters in PETSc must be protected by type-testing
       All PETSc objects have a type name and here we compare against the one of PCSHELL */
    PetscCall(PetscObjectTypeCompare((PetscObject)pc, PCSHELL, &isshell));
    if (isshell) {
      MyPC *myctx;

      PetscCall(PCShellGetContext(pc, &myctx));
      if (!myctx) {
        /* PCSHELL does not have a custom creation routines but needs the
           context passed at construction time. We take a lazy approach,
           and create the context on the fly the first time we are asked for it */
        PetscCall(PetscNew(&myctx));
        PetscCall(PCShellSetContext(pc, myctx));
      }
      *(void **)ctx = myctx;
    }
  }

  /* This is a getter and the caller has requested some data. Here we decide that
     if we cannot retrieve the data, we raise an error on the communicator of the
     preconditioner, with error_code PETSC_ERR_USER (see petscerror.h if you want
     more details) and an error string */
  if (!(*(void **)ctx)) SETERRQ(PetscObjectComm((PetscObject)pc), PETSC_ERR_USER, "Don't know how to get a context");
  PetscFunctionReturn(PETSC_SUCCESS);
}
