/*
   Solve a constant coefficient Poisson problem on a regular grid

     - u_xx - u_yy = 1 in [0,1]^2
     u = 0 on the boundary

   This is a naive, parallel implementation, using n interior grid points per
   dimension and a lexicographic ordering of the nodes.
   Look at seq.c before looking at this example.

   This code is kept as simple as possible to show the minimal differences
   with the sequential version. However, simplicity comes at a price.
   Here we use a naive decomposition that does not lead to an optimal communication
   complexity for the matrix-vector product. See dmda.c for an example of optimal
   communication complexity and nodes ordering.

   In this example we also show how to register a user-defined class of
   preconditioners.

*/

#include <petscksp.h>

/* This example uses PCCreate_User */
#include "mypc.h"

/* Convenience function that maps from global ordering to grid indexing */
void index_to_grid(PetscInt r, PetscInt n, PetscInt *i, PetscInt *j)
{
  *i = r / n;
  *j = r % n;
}

int main(int argc, char *argv[])
{
  KSP         ksp;
  PC          pc;
  Mat         A;
  Vec         x, b;
  PetscInt    n = 5, rstart, rend;
  PetscScalar h, hm2;
  PetscMPIInt size;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  /* Register our preconditioner type
     This must be called by all processes in the PETSC_COMM_WORLD communicator.
     Advanced libraries using PETSc usually register their types at initialization time.
     Now we can select our preconditioner at command line via '-pc_type mypc'
     or programmatically via PCSetType(pc,"mypc"). */
  PetscCall(PCRegister("mypc", PCCreate_User));

  /* Get number of interior points per dimension from command line */
  PetscCall(PetscOptionsGetInt(NULL, NULL, "-n", &n, NULL));

  /* Grid spacing per dimension */
  h   = 1.0 / (n + 1);
  hm2 = 1.0 / (h * h);

  /* Create a parallel sparse matrix object
     We associate the matrix to the WORLD communicator
     Each process (if run with mpiexec -n ) will own a separate
     portion of the parallel object. We let PETSc decide how to split the rows
     and column spaces among processes.
     Since we are using lexicographic ordering, this will lead
     to a completely unoptimal parallel decomposition.
  */
  PetscCall(MatCreate(PETSC_COMM_WORLD, &A));
  PetscCall(MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, n * n, n * n));

  /* MATAIJ is a convenience type that translates to MATSEQAIJ if commsize == 1,
     otherwise it is MATMPIAIJ. This allows us to write a distribution independent code */
  PetscCall(MatSetType(A, MATAIJ));

  /* Allow command line customization */
  PetscCall(MatSetFromOptions(A));

  /* Matrix preallocation. When running in parallel, the matrix type (unless
     changed at command line) will be MATMPIAIJ, and the first call will be
     dummy, since these calls inside use a PetscTryMethod approach and look for
     "MatSeqAIJSetPreallocation_C" and "MatMPIAIJSetPreallocation_C" respectively.
     See seq.c for a more complete discussion on this.
     This approach allows us to write the code once and never recompile for
     different matrix types as long as they support any of the API used here.

     For example, a different type need only to compose their own implementation via
          PetscObjectCompose(...,"MatMPIAIJSetPreallocation_C",myfunc)
     and PETSc will automatically call it.
  */
  PetscCall(MatSeqAIJSetPreallocation(A, 5, NULL));
  PetscCall(MatMPIAIJSetPreallocation(A, 5, NULL, 5, NULL));

  /* All PETSc parallel matrices and vectors are logically partitioned by
     contiguous chunks of rows and columns across the processors.
     In this example each process inserts only row elements that it owns locally
  */
  PetscCall(MatGetOwnershipRange(A, &rstart, &rend));

  /* Insert matrix entries */
  for (PetscInt row = rstart; row < rend; row++) {
    PetscScalar v;
    PetscInt    column, i, j;

    /* from global index to grid indexing */
    index_to_grid(row, n, &i, &j);

    v = -1.0 * hm2;
    if (i > 0) { /* not on left boundary */
      column = row - n;
      PetscCall(MatSetValues(A, 1, &row, 1, &column, &v, INSERT_VALUES));
    }
    if (i < n - 1) { /* not on right boundary */
      column = row + n;
      PetscCall(MatSetValues(A, 1, &row, 1, &column, &v, INSERT_VALUES));
    }
    if (j > 0) { /* not on bottom boundary */
      column = row - 1;
      PetscCall(MatSetValues(A, 1, &row, 1, &column, &v, INSERT_VALUES));
    }
    if (j < n - 1) { /* not on top boundary */
      column = row + 1;
      PetscCall(MatSetValues(A, 1, &row, 1, &column, &v, INSERT_VALUES));
    }
    v = 4.0 * hm2;
    PetscCall(MatSetValues(A, 1, &row, 1, &row, &v, INSERT_VALUES));
  }

  /* Assemble matrix. Note that if we call MatSetValues for a non-owned row,
     the data will be automatically sent by PETSc to the appropriate process
     during matrix assembly. In particular, MatAssemblyBegin will prepare
     the buffers and initiate the communications, while MatAssemblyEnd will
     finalize communications and insert the received locally owned portion.
     Begin/End phases allow for computation to be performed in between. */
  PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));

  /* View matrix with command line option '-view_mat' */
  PetscCall(MatViewFromOptions(A, NULL, "-view_mat"));

  PetscCallMPI(MPI_Comm_size(PetscObjectComm((PetscObject)A), &size));
  if (size > 1) {
    /* parallel sparse matrices are split in diagonal and off-diagonal parts
       e.g with 3 processes and using global ordering:

        rank 0   Diag | Offp | Offp
        rank 1   Offp | Diag | Offp
        rank 2   Offp | Offp | Diag

       Diag store the local, on-process contributions, Offp the off-process contributions to the local vector output.
       The Offp matrices are merged together into a single sequential matrix.
       A good decomposition will produce very sparse Offp matrices.
       Let's check for our decomposition. See also dmda.c
    */
    Mat         Aoff;
    PetscInt    ncolumns_off, nrows_off;
    PetscReal   ratio, *ratios;
    PetscMPIInt rank;

    /* Access the local matrices, here we only want the off-process matrix */
    PetscCall(MatMPIAIJGetSeqAIJ(A, NULL, &Aoff, NULL));

    /* The number of off-process columns will tell us the amount
       of data needed to perform parallel matrix-vector products. */
    PetscCall(MatGetSize(Aoff, &nrows_off, &ncolumns_off));

    /* Collect statistics */
    ratio = 1.0 * ncolumns_off / nrows_off;
    PetscCall(PetscMalloc1(size, &ratios));
    PetscCallMPI(MPI_Comm_rank(PetscObjectComm((PetscObject)A), &rank));
    PetscCallMPI(MPI_Gather(&ratio, 1, MPIU_REAL, ratios, 1, MPIU_REAL, 0, PetscObjectComm((PetscObject)A)));
    if (rank == 0) {
      PetscReal min_off  = PETSC_MAX_REAL; /* A macro for the largest representable real */
      PetscReal max_off  = PETSC_MIN_REAL; /* A macro for the smallest representable real */
      PetscReal mean_off = 0.0;
      for (PetscMPIInt i = 0; i < size; i++) {
        PetscReal r = ratios[i];
        min_off     = PetscMin(min_off, r); /* A macro for the minimum between two values */
        max_off     = PetscMax(max_off, r); /* A macro for the maximum between two values */
        mean_off += r;
      }
      mean_off /= size;
      PetscCall(PetscPrintf(PetscObjectComm((PetscObject)A), "  Offdiagonal stats: MIN %g MAX %g MEAN %g\n", (double)min_off, (double)max_off, (double)mean_off));
    }
    PetscCall(PetscFree(ratios));
  }

  /* Create Krylov solver context */
  PetscCall(KSPCreate(PetscObjectComm((PetscObject)A), &ksp));
  PetscCall(KSPSetType(ksp, KSPCG));
  PetscCall(KSPGetPC(ksp, &pc));
  PetscCall(PCSetType(pc, PCGAMG));
  PetscCall(KSPSetOperators(ksp, A, A));
  PetscCall(KSPSetFromOptions(ksp));

  /* Solve the linear system A x = b , with b = 1 */
  PetscCall(MatCreateVecs(A, &x, &b));
  PetscCall(VecSet(b, 1.0));
  PetscCall(KSPSolve(ksp, b, x));

  /* View solution with command line option '-view_sol' */
  PetscCall(VecViewFromOptions(x, NULL, "-view_sol"));

  /* Destroy PETSc objects that are no longer needed */
  PetscCall(KSPDestroy(&ksp));
  PetscCall(MatDestroy(&A));
  PetscCall(VecDestroy(&x));
  PetscCall(VecDestroy(&b));

  /* Finalize */
  PetscCall(PetscFinalize());
  return 0;
}
