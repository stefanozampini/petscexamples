/*

   Solve a constant coefficient Poisson problem on a regular grid

     - u_xx - u_yy = 1 in [0,1]^2
     u = 0 on the boundary

   This is a parallel implementation using PETSc's regular grid object (DMDA).
   Boundary points are represented.

   Take a look at seq.c and par.c if not yet done.
   In this example we take advantage of DMDA API for grid management.

   We also introduce the concept of solver monitoring, and give an example on how
   it can be integrated into existing PETSc solvers.

   Since we are using a DMDA for grid management, the
   solution can be exported for visualization to Paraview using, e.g.:

      $ ./dmda -da_grid_x 12 -da_grid_y 12 -view_sol vtk:sol.vts:

   See ../eigs/laplacian.c for an example using PETSc's unstructured grid
   object DMPLEX.
*/

/* Include DMDA specific API and its dependencies */
#include <petscdmda.h>
#include <petscksp.h>

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* A convenience function to assemble the linear system right-hand side
   We use the same rhs as in seq.c and par.c, but we have to set to zero
   the boundary nodes. */
static PetscErrorCode AssembleRHS(Vec U)
{
  DM            dm;
  PetscScalar **u;
  PetscInt      xs, ys, xm, ym, mx, my;

  PetscFunctionBeginUser;
  /* First set all entries to 1.0 */
  PetscCall(VecSet(U, 1.0));

  /* we can access the DM from the Vec if the vector has been created
     with DM API like DMCreateGlobalVector() */
  PetscCall(VecGetDM(U, &dm));

  /* Retrieve global indices of the lower left corner of the
     locally owned portion of the grid, and the global grid sizes.
     -> These calls generates an error if dm is not of type DMDA */
  PetscCall(DMDAGetCorners(dm, &xs, &ys, NULL, &xm, &ym, NULL));
  PetscCall(DMDAGetInfo(dm, 0, &mx, &my, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

  /* DMDA offers the possibility of grid indexing a local array */
  PetscCall(DMDAVecGetArray(dm, U, &u));

  /* Set to zero on the domain boundary */
  for (PetscInt j = ys; j < ys + ym; ++j)
    for (PetscInt i = xs; i < xs + xm; ++i)
      if (i == 0 || j == 0 || i == mx - 1 || j == my - 1) u[j][i] = 0.0; /* <- grid indexing */

  /* Tell PETSc we are done working on the local array of U */
  PetscCall(DMDAVecRestoreArray(dm, U, &u));

  PetscFunctionReturn(PETSC_SUCCESS);
}

/* A convenience function to assemble the linear system matrix */
static PetscErrorCode AssembleMatrix(Mat A)
{
  DM          dm;
  PetscInt    xs, ys, xm, ym, mx, my;
  PetscScalar hx, hy, hxm2, hym2;

  PetscFunctionBeginUser;
  /* We can access the DM from the Mat if the latter has been created with
     DMCreateMatrix() */
  PetscCall(MatGetDM(A, &dm));

  /* Compute grid size (this specific example assumes uniform coordinates) */
  PetscCall(DMDAGetInfo(dm, 0, &mx, &my, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
  hx   = 1.0 / (PetscReal)(mx - 1);
  hy   = 1.0 / (PetscReal)(my - 1);
  hxm2 = 1. / (hx * hx);
  hym2 = 1. / (hy * hy);

  /*
     Compute entries for the locally owned part of the matrix.
     Here, we set all entries for a particular row at once using
     MatSetValuesStencil, which internally remaps stencil indices to global
     matrix indices.
  */
  PetscCall(DMDAGetCorners(dm, &xs, &ys, 0, &xm, &ym, 0));
  for (PetscInt j = ys; j < ys + ym; j++) {
    for (PetscInt i = xs; i < xs + xm; i++) {
      MatStencil  col[5], row;
      PetscScalar v[5];
      PetscInt    k = 0;

      row.j = j;
      row.i = i;
      if (i == 0 || j == 0 || i == mx - 1 || j == my - 1) { /* boundary points */
        v[0] = 1.0;
        PetscCall(MatSetValuesStencil(A, 1, &row, 1, &row, v, INSERT_VALUES));
      } else { /* interior grid points */

        if (j - 1 != 0) { /* not neighbor of bottom boundary */
          v[k]     = -hym2;
          col[k].j = j - 1;
          col[k].i = i;
          k++;
        }
        if (i - 1 != 0) { /* not neighbor of left boundary */
          v[k]     = -hxm2;
          col[k].j = j;
          col[k].i = i - 1;
          k++;
        }

        v[k]     = 2.0 * (hxm2 + hym2);
        col[k].j = row.j;
        col[k].i = row.i;
        k++;

        if (i + 1 != mx - 1) { /* not neighbor of the right boundary */
          v[k]     = -hxm2;
          col[k].j = j;
          col[k].i = i + 1;
          k++;
        }
        if (j + 1 != my - 1) { /* not neighbor of the top boundary */
          v[k]     = -hym2;
          col[k].j = j + 1;
          col[k].i = i;
          k++;
        }
        PetscCall(MatSetValuesStencil(A, 1, &row, k, col, v, INSERT_VALUES));
      }
    }
  }

  /* Assemble matrix */
  PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));

  /* Report on offdiagonal ratio (see par.c) */
  PetscMPIInt size;
  PetscCallMPI(MPI_Comm_size(PetscObjectComm((PetscObject)A), &size));
  if (size > 1) {
    Mat         Aoff;
    PetscInt    ncolumns_off, nrows_off;
    PetscReal   ratio, *ratios;
    PetscMPIInt rank;

    PetscCall(MatMPIAIJGetSeqAIJ(A, NULL, &Aoff, NULL));
    PetscCall(MatGetSize(Aoff, &nrows_off, &ncolumns_off));
    ratio = 1.0 * ncolumns_off / nrows_off;
    PetscCall(PetscMalloc1(size, &ratios));
    PetscCallMPI(MPI_Comm_rank(PetscObjectComm((PetscObject)A), &rank));
    PetscCallMPI(MPI_Gather(&ratio, 1, MPIU_REAL, ratios, 1, MPIU_REAL, 0, PetscObjectComm((PetscObject)A)));
    if (rank == 0) {
      PetscReal min_off  = PETSC_MAX_REAL;
      PetscReal max_off  = PETSC_MIN_REAL;
      PetscReal mean_off = 0.0;
      for (PetscMPIInt i = 0; i < size; i++) {
        PetscReal r = ratios[i];
        min_off     = PetscMin(min_off, r);
        max_off     = PetscMax(max_off, r);
        mean_off += r;
      }
      mean_off /= size;
      PetscCall(PetscPrintf(PetscObjectComm((PetscObject)A), "  Offdiagonal stats: MIN %g MAX %g MEAN %g\n", (double)min_off, (double)max_off, (double)mean_off));
    }
    PetscCall(PetscFree(ratios));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* Callbacks for user defined Monitoring routines.
   These functions will be called by PETSc at each iteration of the Krylov solver
   right before the convergence check has been performed. 'it' is the current
   iteration number, 'rnorm' the residual norm of the Krylov method and 'ctx'
   a user defined context
   With this function, we only log iteration number and residual norm at standard
   output.
*/
PetscErrorCode MyMonitor(KSP ksp, PetscInt it, PetscReal rnorm, void *ctx)
{
  PetscFunctionBeginUser;
  PetscCall(PetscPrintf(PetscObjectComm((PetscObject)ksp), "MyMonitor %d: %g\n", (int)it, (double)rnorm));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* With this monitoring routine, we instead dump the solution and residual at some iterations */
PetscErrorCode MyMonitorVTK(KSP ksp, PetscInt it, PetscReal rnorm, void *ctx)
{
  DM          dm;
  Mat         A;
  Vec         r, s;
  PetscInt    print_every = *(PetscInt *)ctx;
  char        filename[PETSC_MAX_PATH_LEN];
  PetscViewer viewer;

  PetscFunctionBeginUser;
  if (it % print_every) PetscFunctionReturn(PETSC_SUCCESS); /* return if we don't need to dump */

  /* Use KSP API to access the operator */
  PetscCall(KSPGetOperators(ksp, &A, NULL));

  /* Access DM for grid information */
  PetscCall(MatGetDM(A, &dm));

  /* Pull out scratch vectors from the DM */
  PetscCall(DMGetGlobalVector(dm, &r));
  PetscCall(DMGetGlobalVector(dm, &s));

  /* Use KSP API to compute current solution s = x_k and residual r = A*x_k - b
     Some methods do not store them, but they need to reconstruct it (like GMRES) */
  PetscCall(KSPBuildSolution(ksp, s, &s));
  PetscCall(KSPBuildResidual(ksp, r, NULL, &r));

  /* Create VTK type PetscViewer to dump the solution */
  PetscCall(PetscSNPrintf(filename, sizeof(filename), "ksp_res_sol_%d.vts", (int)it));
  PetscCall(PetscPrintf(PetscObjectComm((PetscObject)ksp), "MyMonitorVTK: dumping %s\n", filename));
  PetscCall(PetscViewerCreate(PetscObjectComm((PetscObject)ksp), &viewer));
  PetscCall(PetscViewerSetType(viewer, PETSCVIEWERVTK));
  PetscCall(PetscViewerFileSetMode(viewer, FILE_MODE_WRITE));
  PetscCall(PetscViewerFileSetName(viewer, filename));
  PetscCall(PetscViewerPushFormat(viewer, PETSC_VIEWER_VTK_VTS));

  /* View the objects */
  PetscCall(PetscObjectSetName((PetscObject)r, "residual"));
  PetscCall(PetscObjectSetName((PetscObject)s, "solution"));
  PetscCall(VecView(r, viewer));
  PetscCall(VecView(s, viewer));

  /* Destroy the viewer */
  PetscCall(PetscViewerPopFormat(viewer));
  PetscCall(PetscViewerDestroy(&viewer));

  /* Restore scratch vectors */
  PetscCall(DMRestoreGlobalVector(dm, &s));
  PetscCall(DMRestoreGlobalVector(dm, &r));
  PetscFunctionReturn(PETSC_SUCCESS);
}

int main(int argc, char **argv)
{
  DM        dm;
  KSP       ksp;
  PC        pc;
  Mat       A;
  Vec       x, b;
  PetscInt  vtk;
  PetscBool mymonitor;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  /* Read command line options to activate monitors (off by default) */
  mymonitor = PETSC_FALSE;
  PetscCall(PetscOptionsGetBool(NULL, NULL, "-use_monitor", &mymonitor, NULL));
  vtk = 0;
  PetscCall(PetscOptionsGetInt(NULL, NULL, "-monitor_vtk_every", &vtk, NULL));

  /* Create distributed array (DMDA) to manage parallel grid and vectors.
     Here we use a default 7x7 grid to match the discretization of seq.c and
     par.c. We can change the size of the grid with the command line options
     '-da_grid_x' and '-da_grid_y' */
  PetscCall(DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, /* type of boundaries */
                         DMDA_STENCIL_STAR,                                    /* stencil type */
                         7, 7, PETSC_DECIDE, PETSC_DECIDE,                     /* global and local sizes */
                         1, 1,                                                 /* number of dofs per node and stencil width */
                         NULL, NULL,                                           /* custom local sizes */
                         &dm));
  PetscCall(DMSetFromOptions(dm));
  PetscCall(DMSetUp(dm));
  PetscCall(DMDASetUniformCoordinates(dm, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0));

  /* View DM with command line option '-view_dm' */
  PetscCall(DMViewFromOptions(dm, NULL, "-view_dm"));

  /* Create global vectors and matrix from the DM */
  PetscCall(DMCreateGlobalVector(dm, &x));
  PetscCall(DMCreateGlobalVector(dm, &b));
  PetscCall(DMCreateMatrix(dm, &A));

  /* Assemble linear system */
  PetscCall(AssembleRHS(b));
  PetscCall(AssembleMatrix(A));

  /* Create Krylov solver context */
  PetscCall(KSPCreate(PETSC_COMM_WORLD, &ksp));
  PetscCall(KSPSetType(ksp, KSPCG));
  PetscCall(KSPSetOperators(ksp, A, A));

  /* Pass monitoring routines to PETSc. They will be invoked
     in the order they are set. */
  if (mymonitor) PetscCall(KSPMonitorSet(ksp, MyMonitor, NULL, NULL));
  if (vtk) PetscCall(KSPMonitorSet(ksp, MyMonitorVTK, &vtk, NULL));

  /* We can call destroy on A if it is no longer needed
     within this compilation unit.
     In this case, the object is not actually destroyed,
     since the KSP references it.
     -> All PetscObjects are reference counted <- */
  PetscCall(MatDestroy(&A));

  /* Some preconditioners can take advantage of the grid structure.
     In this example we can use multigrid with command line options, e.g.

        -da_refine 5 -pc_type mg -pc_mg_levels 3 -pc_mg_galerkin

     5 levels of refinement, 3 multigrid levels, using Galerkin projection for
     the level operators.
  */
  PetscCall(KSPGetPC(ksp, &pc));
  PetscCall(PCSetDM(pc, dm));

  /* Our default choice matches seq.c and par.c */
  PetscCall(PCSetType(pc, PCGAMG));
  PetscCall(KSPSetFromOptions(ksp));

  /* Solve linear system */
  PetscCall(KSPSolve(ksp, b, x));

  /* View solution with command line option '-view_sol' */
  PetscCall(VecViewFromOptions(x, NULL, "-view_sol"));

  /* Destroy PETSc objects that are no longer needed */
  PetscCall(DMDestroy(&dm));
  PetscCall(KSPDestroy(&ksp));
  PetscCall(VecDestroy(&x));
  PetscCall(VecDestroy(&b));

  /* Finalize */
  PetscCall(PetscFinalize());
  return 0;
}
