/*
   Solve a constant coefficient Poisson problem on a regular grid

     - u_xx - u_yy = 1 in [0,1]^2
     u = 0 on the boundary

   This is a sequential implementation, using n interior grid points per
   dimension and a lexicographic ordering of the nodes.
   Boundary values are not represented. The row stencil for the u_{i,j} grid point is given by

   h^-2 * ( 4.0 * u_{i,j} - u_{i, j-1} - u_{i, j+1} - u_{i-1, j} - u_{i+1,j} )

   with h the grid size and u_{i,j}.

   Y is the fastest indexing dimension, X the slowest.

       y
       ^
       |
       |
     j |........*
       |        .
       |        .
       |        .
     --|---------------> x
       |
       |        i

   In this example we also show how to programmatically customize a user-defined preconditioner using
   the PCSHELL type.

   If you want to use the Python defined preconditioner implemented in mypc.py, run with:

   $ ./seq -python -pc_type python -pc_python_type mypc.mypc

   The first command line option tells PETSc to initialize the bridge with petsc4py during
   PetscInitialize() and register the Python-aware types in PETSc. We select the
   Python aware PC type (PCPYTHON) with the second command line. Finally we tell PETSc where to look for
   the implementation with the last option. Note that mypc.py should either be in the local
   folder, or be visible via PYTHONPATH.

*/

/* Include KSP (Krylov solver) specific API and its dependencies.
   This automatically includes matrices and vectors */
#include <petscksp.h>

/* This example uses functions defined in a user-defined header */
#include "mypc.h"

int main(int argc, char *argv[])
{
  KSP         ksp;  /* linear solver object */
  PC          pc;   /* preconditioner */
  Mat         A;    /* matrix object */
  Vec         x, b; /* vectors */
  PetscInt    n = 5;
  PetscScalar h, hm2;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  /* Get number of interior points per dimension from command line */
  PetscCall(PetscOptionsGetInt(NULL, NULL, "-n", &n, NULL));

  /* Grid spacing per dimension */
  h   = 1.0 / (n + 1);
  hm2 = 1.0 / (h * h);

  /* Create a sequential sparse matrix object
     We associate the matrix with the SELF communicator
     Each process (if run with mpiexec -n ) will own a separate
     instance of the object */
  PetscCall(MatCreate(PETSC_COMM_SELF, &A));
  PetscCall(MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, n * n, n * n));
  PetscCall(MatSetType(A, MATSEQAIJ));

  /* Allow object customization at command line */
  PetscCall(MatSetFromOptions(A));

  /* Preallocate the matrix with 5 nonzeros per row.
     Try setting to a smaller value and recompile.
     Run using -info :mat to get additional information */
  PetscCall(MatSeqAIJSetPreallocation(A, 5, NULL));

  /* Insert matrix entries */
  for (PetscInt i = 0; i < n; i++) {
    for (PetscInt j = 0; j < n; j++) {
      PetscScalar v;
      PetscInt    row, column;

      v   = -1.0 * hm2;
      row = j + n * i;
      if (i > 0) { /* not on left boundary */
        column = row - n;
        PetscCall(MatSetValues(A, 1, &row, 1, &column, &v, INSERT_VALUES));
      }
      if (i < n - 1) { /* not on right boundary */
        column = row + n;
        PetscCall(MatSetValues(A, 1, &row, 1, &column, &v, INSERT_VALUES));
      }
      if (j > 0) { /* not on bottom boundary */
        column = row - 1;
        PetscCall(MatSetValues(A, 1, &row, 1, &column, &v, INSERT_VALUES));
      }
      if (j < n - 1) { /* not on top boundary */
        column = row + 1;
        PetscCall(MatSetValues(A, 1, &row, 1, &column, &v, INSERT_VALUES));
      }
      v = 4.0 * hm2;
      PetscCall(MatSetValues(A, 1, &row, 1, &row, &v, INSERT_VALUES));
    }
  }

  /* Assemble matrix */
  PetscCall(MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY));

  /* View matrix with command line option '-view_mat' */
  PetscCall(MatViewFromOptions(A, NULL, "-view_mat"));

  /* Create Krylov solver context and choose the preconditioned conjugate
     gradient method with AMG preconditioner as a solver.
     Such source code customization can be overridden from command line, e.g.

            '-ksp_type gmres -pc_type bjacobi'

     We associate the same communicator used for the matrix.
  */
  PetscCall(KSPCreate(PetscObjectComm((PetscObject)A), &ksp));
  PetscCall(KSPSetType(ksp, KSPCG));
  PetscCall(KSPGetPC(ksp, &pc));    /* Access the preconditioner */
  PetscCall(PCSetType(pc, PCGAMG)); /* Preconditioner customization */

  /* Pass operators to PETSc. KSP accepts two operators
   - the first is the matrix that will be used by the Krylov solver
   - the second is the matrix that will be used to build the preconditioner
     In this case they are identical */
  PetscCall(KSPSetOperators(ksp, A, A));

  /* Customize ksp from command line */
  PetscCall(KSPSetFromOptions(ksp));

  /* If we selected the 'shell' type at command line, the next calls will
     customize the preconditioner using our-own functions. Note that these calls
     are dummy calls if the preconditioner is not of type PCSHELL. If we look at
     the interface API in "${PETSC_DIR}/src/ksp/pc/impls/shell/shellpc.c"
     we can see that they use PetscTryMethod on the pc object with a code like
     this:

        PetscTryMethod(pc, "PCShellSetApply_C", ....)

     PetscTryMethod calls PetscQueryFunction, that inspects a private map of each
     PetscObject of the type string -> function_pointer. If "PCShellSetApply_C" corresponds to a
     function pointer, the function is called. Otherwise, control is returned to
     the caller.
  */
  PetscCall(PCShellSetApply(pc, PCApply_User));
  PetscCall(PCShellSetDestroy(pc, PCDestroy_User));
  PetscCall(PCShellSetSetUp(pc, PCSetUp_User));

  /* Create vectors suitable for the matrix-vector multiplication A * x = b,
     using the Mat API, thus we will always have the correct type of vector,
     independently on the matrix type */
  PetscCall(MatCreateVecs(A, &x, &b));

  /* Solve the linear system A x = b , with b = 1
     Specifying '-ksp_view' will cause PETSc to dump information about the
     linear solver context at the end of the iterative solution process.
     Use '-help' and grep for 'ksp_' to see available options.

     We can monitor the iterative solution process using default monitors
     from command line. Monitors can be also added programmatically and be user-defined,
     see dmda.c for additional information.

     Try running with '-ksp_monitor', or '-ksp_monitor_short'.
     Use '-help' and grep for 'ksp_monitor' to see available options.

  */
  PetscCall(VecSet(b, 1.0));
  PetscCall(KSPSolve(ksp, b, x));

  /* View solution with command line option '-view_sol'.
     This is a dummy call if the option is not specified */
  PetscCall(VecViewFromOptions(x, NULL, "-view_sol"));

  /* Destroy PETSc objects that are no longer needed */
  PetscCall(KSPDestroy(&ksp));
  PetscCall(MatDestroy(&A));
  PetscCall(VecDestroy(&x));
  PetscCall(VecDestroy(&b));

  /* Finalize */
  PetscCall(PetscFinalize());
  return 0;
}
