This folder contains three small applications that solve the constant coefficient Poisson problem on a regular grid, possibly using custom user-defined preconditioners written in C or in Python.

- [seq.c](seq.c): Is a sequential implementation that introduces the concepts of matrix preallocation and linear system solvers. It also describes how to set up a user-defined custom preconditioner using the PCSHELL type.

- [par.c](par.c): Is the naive parallel version of seq.c. This example reuses the same user-defined custom preconditioner, but instead of using the PCSHELL type, it registers a new PC type that can be selected by PETSc.

- [dmda.c](dmda.c): Is an efficient implementation based on PETSc's structured grid object DMDA that integrates nicely with visualization tools like Paraview. The example also introduces the concept of monitor in a solver.

- [par.py](par.py): A Python version of par.c

There are additional files that contains the needed code to specify the user-defined preconditioners, and provide additional information on lower-level details of PETSc. In particular:

- [mypc.c](mypc.c): contains the private implementation of the preconditioner methods in C.

- [mypc_type.c](mypc_type.c): shows how to populate the PC object data structure with the type specific methods when we register a new PC type.

- [mypc.py](mypc.py): describes how to implement a preconditioner in Python that can be called by PETSc.
