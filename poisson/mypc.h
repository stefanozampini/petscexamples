/* This file contains prototypes for functions in mypc.c */

/* It is always good practice to guard against double-inclusion
   The alternative is to use the non-standard (albeit widely adopted) macro

     #pragma once

   Best coding practices should avoid the use of non-standard components of the
   language.
*/
#ifndef __MYPC_H_
#define __MYPC_H_

/* Include the PC class and all its dependencies */
#include <petscpc.h>

PETSC_EXTERN PetscErrorCode PCSetUp_User(PC);
PETSC_EXTERN PetscErrorCode PCApply_User(PC, Vec, Vec);
PETSC_EXTERN PetscErrorCode PCDestroy_User(PC);
PETSC_EXTERN PetscErrorCode PCCreate_User(PC);
#endif
