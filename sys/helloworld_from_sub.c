/*
   Concepts:
     PETSc initialization on subcommunicators
     MPI variables/API from PETSc
*/
#include <mpi.h>

/* It is always good practice to check error codes
   PETSc provides PetscCallMPI which internally does a very similar thing we do here.
   We use the below macro only before and after PetscInitialize/PetscFinalize
   are called, since PETSc will not be available */
#define CheckMPI(a) \
  do { \
    int  errorcode; \
    char errorstring[MPI_MAX_ERROR_STRING]; \
    errorcode = a; \
    if (errorcode) { \
      int len; \
      MPI_Error_string(errorcode, (char *)errorstring, &len); \
      fprintf(stderr, "MPI error %d %s", errorcode, errorstring); \
    } \
  } while (0)

#include <petsc.h>

int main(int argc, char *argv[])
{
  PetscMPIInt mpirank, mpisize, mpicolor;
  PetscMPIInt prank, psize;

  /* Initialize MPI before PETSc */
  CheckMPI(MPI_Init(&argc, &argv));
  CheckMPI(MPI_Comm_size(MPI_COMM_WORLD, &mpisize));
  CheckMPI(MPI_Comm_rank(MPI_COMM_WORLD, &mpirank));

  /* Split MPI_COMM_WORLD in odd and even ranks communicators and
     assign the resulting comm to PETSC_COMM_WORLD. */
  mpicolor = mpirank % 2;
  CheckMPI(MPI_Comm_split(MPI_COMM_WORLD, mpicolor, mpirank, &PETSC_COMM_WORLD));

  /* Initialize PETSc: there will be two different PETSc instances running
     One will run using even ranks in MPI_COMM_WORLD, the other using odd ranks */
  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  PetscCallMPI(MPI_Comm_size(PETSC_COMM_WORLD, &psize));
  PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &prank));
  PetscCall(PetscPrintf(PETSC_COMM_SELF, "Hello from PETSc process id %d (out of %d), Split group %d, World rank and size %d %d\n", prank, psize, mpicolor, mpirank, mpisize));

  /* Finalize PETSc */
  PetscCall(PetscFinalize());

  /* We are responsible to free PETSC_COMM_WORLD since it has not been created by PETSc */
  CheckMPI(MPI_Comm_free(&PETSC_COMM_WORLD));

  /* We are responsible to finalize MPI */
  CheckMPI(MPI_Finalize());
  return 0;
}
