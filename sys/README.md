This directory contains some examples to help you familiarize with basic PETSc stuff.

- [makefile](makefile): how to use PETSc makefile variables to compile your code

- [helloworld.c](helloworld.c): a simple C program that initializes PETSc, calls MPI and prints something at standard output

- [helloworld.py](helloworld.py): a simple Python program that initializes PETSc, and prints something at standard output

- [helloworld_from_sub.c](helloworld_from_sub.c): shows how to initialize PETSc on subcommunicators

- [helloworld_with_dep.c](helloworld_with_dep.c): shows how to call external functions and PETSc coding practices to support different configurations

- [error.c](error.c): a C program showing PETSc error handling capabilities

- [error_cpp.cpp](error_cpp.cpp): a C++ program showing how to catch exceptions and their integration with PETSc error handling capabilities

- [object](object.c): discusses concepts about PETSc's design for object orientation and constructs a new class step-by-step.
