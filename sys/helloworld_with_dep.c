/*
   Concepts:
     PETSc initialization
     PETSc built-in types
     external dependencies
*/
#include <petsc.h>

/* declare prototypes for external functions
   Here we use the PETSC_EXTERN convenience macro that expands to 'extern' or 'extern "C"'
   depending on whether we use a C or a C++ compiler to compile this code */
PETSC_EXTERN PetscErrorCode foo(PetscInt *, PetscBool *);       /* this function uses entry/exit points for tracebacks */
PETSC_EXTERN PetscErrorCode mycppf(PetscReal *, PetscScalar *); /* this function uses C++ inside */
PETSC_EXTERN void           bar(int *);                         /* another function, knowing nothing about PETSc, but hopefully callable from C */

int main(int argc, char *argv[])
{
  PetscMPIInt rank, size;     /* PetscMPIInt is the integer type MPI is expecting, usually 'int' */
  PetscInt    foo_out_int;    /* PetscInt is a 32- or 64- bit integer depending on the configuration */
  PetscBool   foo_out_bool;   /* PetscBool is an enum, { PETSC_FALSE = 0, PETSC_TRUE} */
  PetscReal   cpp_out_real;   /* PetscReal holds a real number: this has a different width depending on PETSc configuration (4-, 8- or 16-bytes long) */
  PetscScalar cpp_out_scalar; /* PetscScalar holds a scalar number: it is the same of PetscReal unless PETSc has been compiled with support for complex numbers. In that case, it is a complex number */
  int         bar_out;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));
  PetscCallMPI(MPI_Comm_size(PETSC_COMM_WORLD, &size));
  PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &rank));

  /* foo and mycppf internally use PETSc error handling. In this case we thus
     wrap these calls using PetscCall() */
  PetscCall(foo(&foo_out_int, &foo_out_bool));
  PetscCall(mycppf(&cpp_out_real, &cpp_out_scalar));

  /* bar does not know anything about PETSc, just push/pop its name on the PETSc stack
     In case it fails, PETSc stacktrace will report "bar" has failed */
  PetscStackCallExternalVoid("bar", bar(&bar_out));

  PetscCall(PetscPrintf(PETSC_COMM_SELF, "Hello from process id %d (out of %d): foo_out %s (%d), cpp_out %g (%g + i * %g), bar_out %d\n", rank, size, foo_out_bool ? "True" : "False", (int)foo_out_int, (double)cpp_out_real, (double)PetscRealPart(cpp_out_scalar), (double)PetscImaginaryPart(cpp_out_scalar), bar_out));

  /* Finalize PETSc */
  PetscCall(PetscFinalize());
  return 0;
}
