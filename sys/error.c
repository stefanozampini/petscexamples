/*
   Concepts:
     Error handling in C.
*/
#include <petsc.h>

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* We create an ad-hoc function that will raise an error in PETSc using SETERRQ.
   When run, this program generates a useful stack-trace for easier debugging. Something like

[0]PETSC ERROR: --------------------- Error Message --------------------------------------------------------------
[0]PETSC ERROR: Reached nesting level 4
[0]PETSC ERROR: See https://petsc.org/release/faq/ for trouble shooting.
[0]PETSC ERROR: Petsc Development GIT revision: v3.17.4-1207-g6a8b292d8ae  GIT Date: 2022-09-04 10:14:04 +0300
[0]PETSC ERROR: ./error on a arch-debug named localhost.localdomain by szampini Wed Sep 28 12:08:45 2022
[0]PETSC ERROR: Configure options ...
[0]PETSC ERROR: #1 ErrorGeneratingFunction() at error.c:42
[0]PETSC ERROR: #2 ErrorGeneratingFunction() at error.c:41
[0]PETSC ERROR: #3 ErrorGeneratingFunction() at error.c:41
[0]PETSC ERROR: #4 ErrorGeneratingFunction() at error.c:38
[0]PETSC ERROR: #5 main() at error.c:50
[0]PETSC ERROR: No PETSc Option Table entries
[0]PETSC ERROR: ----------------End of Error Message -------send entire error message to petsc-maint@mcs.anl.gov----------

*/

static int nesting_error_level = 4;
static int nesting_error_cnt   = 0;

PetscErrorCode ErrorGeneratingFunction()
{
  PetscFunctionBeginUser;
  nesting_error_cnt++;
  switch (nesting_error_cnt) {
  case 1:
    PetscCall(ErrorGeneratingFunction()); /* <- #4 in stack trace */
    break;
  default:
    if (nesting_error_cnt < nesting_error_level) PetscCall(ErrorGeneratingFunction());              /* <- #2 and #3 in stack trace */
    else SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Reached nesting level %d", nesting_error_level); /* <- #1 in stack trace */
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

int main(int argc, char *argv[])
{
  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));
  PetscCall(ErrorGeneratingFunction()); /* #5 in stack trace */
  PetscCall(PetscFinalize());
  return 0;
}
