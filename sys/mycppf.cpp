#include <petscsys.h>
#include <petscerror.h>
#include <limits>

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* PetscFunctionBegin/End is available in C++ too.
   If you want to catch C++ exceptions and use PETSc error
   handling, you can use PetscCallCXX. See error_cpp.cpp
   for additional details. */
template <typename T>
PetscErrorCode mycppf_template(T *out)
{
  PetscFunctionBeginUser;
  PetscCallCXX(*out = std::numeric_limits<T>::epsilon());
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* Provide an interface to make the function callable from C */
PETSC_EXTERN PetscErrorCode mycppf(PetscReal *outr, PetscScalar *outc)
{
  PetscFunctionBeginUser;
  PetscCall(mycppf_template<PetscReal>(outr));
  PetscCall(mycppf_template<PetscScalar>(outc));
  PetscFunctionReturn(PETSC_SUCCESS);
}
