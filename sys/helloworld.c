/*
   Concepts:
     PETSc initialization
     MPI variables/API from PETSc
     Error handling
*/
#include <petsc.h>

int main(int argc, char *argv[])
{
  PetscMPIInt rank, size;

  /* All PETSc programs starts with a call to PetscInitialize().
     This function initializes all we need for running PETSc programs.
     It also initializes MPI for us (if not initialized before, see e.g.
     helloworld_from_sub.c) and other low level components.

     Note that we wrap all PETSc routines with PetscCall() to handle
     error handling. This is from version 3.17. Earlier versions of PETSc
     used the pattern

         ierr = _call_a_petsc_function(...);CHKERR(ierr);

  */
  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  /* PETSc defines its own communicators, PETSC_COMM_WORLD and PETSC_COMM_SELF,
     to not interfere with MPI_COMM_WORLD and MPI_COMM_SELF, which should not
     be used directly by application writers as per best-coding standards.

     We wrap direct MPI calls using PetscCallMPI() to properly
     handle error handling. See helloworld_from_sub.c for more
     details on what PetscCallMPI does internally.
  */
  PetscCallMPI(MPI_Comm_size(PETSC_COMM_WORLD, &size));
  PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &rank));

  /* PetscPrintf is a convenience function that prints a string to stdout only
     from rank 0 of the input communicator. In this case, we are using PETSC_COMM_SELF,
     so all processes will print */
  PetscCall(PetscPrintf(PETSC_COMM_SELF, "Hello from process id %d (out of %d)\n", rank, size));

  /* When we are done using PETSc, we need to finalize it so
     that memory will be released and logs (if run using '-log_view') will be dumped */
  PetscCall(PetscFinalize());
  return 0;
}
