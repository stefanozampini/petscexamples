#include <petscsys.h>
#include <petscerror.h>

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* An example of a PETSc-aware C function.
   The code is wrapped within PetscFunctionBegin{User}/PetscFunctionReturn calls.
   These are macros that helps PETSc managing functions stack and error tracebacks. */
PETSC_EXTERN PetscErrorCode foo(PetscInt *iout, PetscBool *out)
{
  PetscFunctionBeginUser;
#if defined FOO_FALSE
  *out  = PETSC_FALSE;
  *iout = (PetscInt)PETSC_FALSE;
#else
  *out  = PETSC_TRUE;
  *iout = (PetscInt)PETSC_TRUE;
#endif
  PetscFunctionReturn(PETSC_SUCCESS);
}
