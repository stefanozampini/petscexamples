#ifdef __cplusplus
extern "C" {
#endif

void bar(int *out)
{
  *out = 6;
  return;
}

#ifdef __cplusplus
}
#endif
