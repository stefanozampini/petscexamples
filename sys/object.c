/*
   Here we register a new class in PETSc, called "Animal", and we use it to discuss the coding design of all the other PETSc classes like "Vec", "Mat", "KSP" etc.

   Concepts:
     PETSc objects, function and object composition
*/

/* This example needs a private header to access private members of a PetscObject */
#include <petsc/private/petscimpl.h>
#include <petscviewer.h>

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* ------------------------------------------------------------------------------------------------------------------
   First describe the public API for the class, see e.g. "$PETSC_DIR/include/petscmat.h". In this example we have:

     - AnimalCreate
     - AnimalDestroy
     - AnimalSetType
     - AnimalView

   For this to work, we forward declare our C "Animal" type as a pointer to struct.
   In PETSc, _p_XXX structs are hidden in the private headers, see e.g. "$PETSC_DIR/include/petsc/private/matimpl.h"
--------------------------------------------------------------------------------------------------------------------- */
typedef struct _p_Animal *Animal;

PetscErrorCode AnimalCreate(MPI_Comm, Animal *);
PetscErrorCode AnimalSetType(Animal, const char *);
PetscErrorCode AnimalView(Animal, PetscViewer);
PetscErrorCode AnimalDestroy(Animal *);

/* A registered class in PETSc is identified by a unique ID */
PetscClassId ANIMAL_CLASSID;

/* ------------------------------------------------------------------------------------------------------------------
  Now the class level private implementation, see e.g. "$PETSC_DIR/include/petsc/private/matimpl.h"
--------------------------------------------------------------------------------------------------------------------- */

/* Every class is associated to a list of functions, the constructors for each different type of the class */
PetscFunctionList AnimalList = NULL;

/* Declare some operations that Animals can do */
struct _AnimalOps {
  PetscErrorCode (*destroy)(Animal);           /* Destroy data associated with the private implementation */
  PetscErrorCode (*view)(Animal, PetscViewer); /* View data associated with the private implementation */
};

/* This is the struct that will hold the implementation of the class.
   It has been designed this way to resemble the structs used for all the other PETSc class.
   The first entry is a PETSCHEADER, the PETSc way of implementing class inheritance (from PetscObject), virtual functions table etc */
struct _p_Animal {
  PETSCHEADER(struct _AnimalOps);

  /* This is an entry common to all the types of Animals */
  char species[32];

  /* Each Animal type can store its private data here */
  void *data;
};

/* ------------------------------------------------------------------------------------------------------------------
   Now the interface code, see e.g. the sources in "$PETSC_DIR/src/mat/interface/"
--------------------------------------------------------------------------------------------------------------------- */

/* The entry point to create an instance of Animal, inheriting from PetscObject. */
PetscErrorCode AnimalCreate(MPI_Comm comm, Animal *a)
{
  Animal b;

  PetscFunctionBeginUser;
  /* We create a new PetscObject of the Animal class, and associate two basic operations: view and destruction.
     Note that each PetscObject is associated to a communicator */
  PetscCall(PetscHeaderCreate(b, ANIMAL_CLASSID, "Animal", NULL, NULL, comm, AnimalDestroy, AnimalView));

  /* We then populate the common part of the struct */
  PetscCall(PetscStrncpy(b->species, "unknown", 32));

  *a = b;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* Destroy the Animal object. */
PetscErrorCode AnimalDestroy(Animal *a)
{
  PetscFunctionBeginUser;
  /* PETSc supports calling Destroy on a NULL object */
  if (!*a) PetscFunctionReturn(PETSC_SUCCESS);

  /* First check if we are given the object class we are expecting.
     This, as many other sanity calls in PETSc, is a no-op when compiled without debugging */
  PetscValidHeaderSpecific(*a, ANIMAL_CLASSID, 1);

  /* Every XXXDestroy function in PETSc performs this very same reference counting,
     and returns if someone else is holding a reference.
     Set the pointer to NULL so that callers will no longer be able to access the object */
  if (--((PetscObject)(*a))->refct > 0) {
    *a = NULL;
    PetscFunctionReturn(PETSC_SUCCESS);
  }

  /* No one references this object, we can delete the memory associated with it */

  /* If a type has private data to destroy, call the destroy function */
  if ((*a)->ops->destroy) PetscCall(((*a)->ops->destroy)(*a));

  /* Destroy the PetscObject */
  PetscCall(PetscHeaderDestroy(a));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* When XXXSetType is called, a new instance of the type is instantiated.
   This means that any previous type-specific implementation of the class methods (see _AnimalOps)
   must be cleared before the new type constructor is called */
PetscErrorCode AnimalSetType(Animal a, const char *animaltype)
{
  PetscBool sametype;
  PetscErrorCode (*r)(Animal);

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(a, ANIMAL_CLASSID, 1);

  /* If type already set and of the same type, return */
  PetscCall(PetscObjectTypeCompare((PetscObject)a, animaltype, &sametype));
  if (sametype) PetscFunctionReturn(PETSC_SUCCESS);

  /* Look in the list of registered constructors using 'animaltype' as key */
  PetscCall(PetscFunctionListFind(AnimalList, animaltype, &r));
  if (!r) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_ARG_UNKNOWN_TYPE, "Unknown Animal type given: %s", animaltype);

  /* We need to destroy the old implementation and reset the object operations
     to be ready for the new constructor to be called */
  if (a->ops->destroy) PetscCall((*a->ops->destroy)(a));
  PetscCall(PetscMemzero(a->ops, sizeof(struct _AnimalOps)));

  /* Change type name */
  PetscCall(PetscObjectChangeTypeName((PetscObject)a, animaltype));

  /* Create the new data structure */
  PetscCall((*r)(a));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* Interface call to View an Animal */
PetscErrorCode AnimalView(Animal a, PetscViewer viewer)
{
  const char *name;
  const char *type;
  PetscBool   isascii;
  PetscObject c;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(a, ANIMAL_CLASSID, 1);
  /* This is the same for all PETSc objects: Get STDOUT if viewer == NULL */
  if (!viewer) PetscCall(PetscViewerASCIIGetStdout(PetscObjectComm((PetscObject)a), &viewer));
  PetscValidHeaderSpecific(viewer, PETSC_VIEWER_CLASSID, 2);

  /* Every PetscObject uses the same type-comparison (based on string comparison)
     Our view implementation is limited to ASCII type viewers */
  PetscCall(PetscObjectTypeCompare((PetscObject)viewer, PETSCVIEWERASCII, &isascii));
  if (!isascii) PetscFunctionReturn(PETSC_SUCCESS);

  /* Since Animal inherits from PetscObject, we can 'downcast' (just cast in C)
     and access the PetscObject API */
  PetscCall(PetscObjectGetName((PetscObject)a, &name)); /* The name we have given to the object */
  PetscCall(PetscObjectGetType((PetscObject)a, &type)); /* The specific animal type */

  /* Common part to all types */
  PetscCall(PetscViewerASCIIPrintf(viewer, "I am a %s, %s, and my name is %s\n", type, a->species, name));
  PetscCall(PetscViewerASCIIPrintf(viewer, "My refcounting is %d\n", (int)((PetscObject)a)->refct));

  /* See if a specific type can provide additional information */
  if (a->ops->view) PetscCall((*a->ops->view)(a, viewer));

  /* Here we query for a composed object using "_composed_object" as key */
  PetscCall(PetscObjectQuery((PetscObject)a, "_composed_object", &c));
  if (c) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "I have an object composed with me\n"));
    PetscCall(PetscViewerASCIIPushTab(viewer));
    /* We don't know what type of object "c" is, so we call the generic View */
    PetscCall(PetscObjectView(c, viewer));
    PetscCall(PetscViewerASCIIPopTab(viewer));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* ------------------------------------------------------------------------------------------------------------------
  Now we provide the implementation for a specific type of the class, the dog.
  See the e.g. "$PETSC_DIR/src/mat/impls/" for the various "Mat" implementations.
--------------------------------------------------------------------------------------------------------------------- */

/* We use a dummy private data only for the sake of exposition */
typedef struct {
  int dummy;
} DogImpl;

PetscErrorCode AnimalView_Dog(Animal a, PetscViewer viewer)
{
  /* No need to perform sanity checks here, since the private impl is accessed only with valid input */
  DogImpl *dog = (DogImpl *)a->data;

  PetscFunctionBeginUser;
  PetscCall(PetscViewerASCIIPrintf(viewer, "  I am dog number %d\n", dog->dummy));
  PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode AnimalDestroy_Dog(Animal a)
{
  PetscFunctionBeginUser;
  /* We must free the memory allocated with PetscNew */
  PetscCall(PetscFree(a->data));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* Dog creation: set function pointers and allocate private data */
PetscErrorCode AnimalCreate_Dog(Animal a)
{
  DogImpl   *dog;
  static int dummy = 0; /* just a stupid way of getting track of how many dogs have been created */

  PetscFunctionBeginUser;
  /* Allocate private data. PetscNew is just a wrapper around calloc(1,sizeof(*dog))
     This is a convenience to automatically zero the allocated memory, very useful
     when using structs with pointers. */
  PetscCall(PetscNew(&dog));
  dog->dummy = dummy++;

  /* Associate this dog private data with _p_Animal struct */
  a->data = dog;

  /* We can populate common parts of the _p_Animal struct */
  PetscCall(PetscStrncpy(a->species, "mammal", 32));

  /* Set type specific implementations */
  a->ops->view    = AnimalView_Dog;
  a->ops->destroy = AnimalDestroy_Dog;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* ------------------------------------------------------------------------------------------------------------------
  Now the main driver code to check our class.
--------------------------------------------------------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
  Animal charlie, snoop;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  /* Register a new class */
  PetscCall(PetscClassIdRegister("Animal", &ANIMAL_CLASSID));

  /* Register the dog type for the Animal class */
  PetscCall(PetscFunctionListAdd(&AnimalList, "dog", AnimalCreate_Dog));

  /* Create two instances of the dog class */
  PetscCall(AnimalCreate(PETSC_COMM_WORLD, &charlie));
  PetscCall(AnimalSetType(charlie, "dog"));
  PetscCall(PetscObjectSetName((PetscObject)charlie, "Charlie"));

  PetscCall(AnimalCreate(PETSC_COMM_WORLD, &snoop));
  PetscCall(AnimalSetType(snoop, "dog"));
  PetscCall(PetscObjectSetName((PetscObject)snoop, "Snoop"));

  /* View the objects */
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "\nViewing Charlie\n"));
  PetscCall(AnimalView(charlie, NULL));
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "\nViewing Snoop\n"));
  PetscCall(AnimalView(snoop, NULL));

  /* PetscObjects can be composed with each other, and reference counting is automatically increased for the composed object.
     Think as it as providing extra information that cannot be passed with public API.
     PETSc internally uses a map string -> PetscObject for this kind of information */
  PetscCall(PetscObjectCompose((PetscObject)charlie, "_composed_object", (PetscObject)snoop));

  /* Now view only charlie, snoop should pop up with increased reference count */
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "\nViewing Charlie after Snoop has been composed\n"));
  PetscCall(AnimalView(charlie, NULL));

  /* Destroy charlie. This will decrement reference counting for snoop */
  PetscCall(AnimalDestroy(&charlie));

  /* Now view snoop, reference counting should be 1 */
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "\nViewing Snoop after Charlie has been destroyed\n"));
  PetscCall(AnimalView(snoop, NULL));
  PetscCall(AnimalDestroy(&snoop));

  /* We need to destroy the list of functions for the classes
     that are not under PETSc control */
  PetscCall(PetscFunctionListDestroy(&AnimalList));
  PetscCall(PetscFinalize());
  return 0;
}
