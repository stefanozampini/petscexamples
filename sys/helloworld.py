# Import sys for command line handling
import sys

# Import petsc4py module
# This has only a few methods, one of them is 'init'
import petsc4py

# Initialize PETSc with command line options
petsc4py.init(sys.argv)

# Import the PETSc module
from petsc4py import PETSc

# PETSc.COMM_WORLD and PETSc.COMM_SELF have methods that
# allow to get rank and size without the need of having mpi4py installed
rank = PETSc.COMM_WORLD.Get_rank()
size = PETSc.COMM_WORLD.Get_size()

# This is the equivalent of PetscPrintf(comm,....)
PETSc.Sys.Print(f'Hello from process id {rank} (out of {size})', comm=PETSc.COMM_SELF)
