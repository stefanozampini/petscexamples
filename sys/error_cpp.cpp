/*
   Concepts:
     Error handling in C++
*/
#include <petsc.h>
#include <stdexcept>

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* A function that will raise an exception caught by PETSc. */
void ExceptionRaisingFunction()
{
  throw std::runtime_error("Cannot call this function");
}

int main(int argc, char *argv[])
{
  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));
  PetscCallCXX(ExceptionRaisingFunction());
  PetscCall(PetscFinalize());
  return 0;
}
