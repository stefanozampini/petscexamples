all:
	$(MAKE) -C sys
	$(MAKE) -C debugging
	$(MAKE) -C eigs
	$(MAKE) -C exercises
	$(MAKE) -C misc
	$(MAKE) -C nonlinear
	$(MAKE) -C poisson

clean::
	$(MAKE) -C sys clean
	$(MAKE) -C debugging clean
	$(MAKE) -C eigs clean
	$(MAKE) -C exercises clean
	$(MAKE) -C misc clean
	$(MAKE) -C nonlinear clean
	$(MAKE) -C poisson clean

GITSRC = '*.[chF]' '*.F90' '*.hpp' '*.cpp' '*.cxx' '*.cu'
clangformat:
	-@git --no-pager ls-files -z ${GITSRC} | xargs -0 -P $(MAKE_NP) -L 10 clang-format -i

ruffformat:
	-@ruff format

format: clangformat ruffformat

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules
