/* compatibility header to be included AFTER petsc includes */

#ifndef PetscCall
#define PetscCall(code) \
  do { \
    PetscErrorCode ierr = (code); \
    CHKERRQ(ierr); \
  } while (0)
#define undefPetscCall
#endif

#if PETSC_VERSION_LT(3, 19, 0)
#define PETSC_SUCCESS 0
#endif
