This short set of commented C/C++ and Python examples tries to give a simple introduction to some of the capabilities of the Portable and Extensible Toolkit for Scientific Computations [PETSc](https://petsc.org/)
as a tool for the design and implementation of high-performant, distributed memory software for the solution
of nonlinear equations and eigenvalue problems.

There are different folders, each showcasing a different concept in PETSc.

- [sys](sys/): PETSc initialization and finalization, error handling, object orientation and makefile variables

- [debugging](debugging/): Some info on how to debug PETSc (not only) programs

- [poisson](poisson/): Three different ways of solving a two-dimensional Poisson problem discretized with finite differences.
                       The examples also discuss different ways of creating a custom preconditioner and register a new type.

- [nonlinear](nonlinear/): These are examples that describe how to setup various solvers in PETSc, showing
                           the solution of a nonlinear partial differential equation (using SNES), time-dependent problems (TS),
                           and minimization problems (TAO).

- [eigs](eigs/): Examples showing how to use [SLEPc](https://slepc.upv.es/), the eigenvalue solver package built on top of PETSc.

- [misc](misc/): Miscellaneous examples, including custom communications for vectors and generic data structures.


Exercises can be found in [exercises](exercises/).

Comments and suggestions are welcome. Please use the issue tracker for those.
If you want to contribute simple examples, open a pull request.
