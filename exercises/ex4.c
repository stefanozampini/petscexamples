/* Solve the Poisson equation -\Delta u = f on [0,1]^2 with Dirichlet boundary conditions.

Copy poisson/dmda.c and modify it to use a higher-order stencil coming from a compact finite difference scheme

1/(6*h^2) * (−20 * u_{i,j} + 4 * u_{i+1,j} + 4 * u_{i−1,j} + 4 * u_{i,j+1} + 4 * u_{i,j-1}
         + u_{i+1,j+1} + u+{i−1,j+1} + u_{i+1,j−1} + u_{i−1,j−1})
= 1/12 * (8 * f_{i,j} + f_{i+1,j} + f_{i−1,j} + f_{i,j+1} + f_{i,j−1})

For each i,j in the interior of the grid, the stencils can be represented graphically as (ignoring the mesh scalings)

      1  4  1             1
lhs = 4 -20 4  , rhs = 1  8  1
      1  4  1             1

Use a Method of Manufactured Solutions (MMS) to check the correctness of the code and to compute the convergence rate as the mesh size h is decreased.
Use f = sin(2*pi*x) * sin(2*pi*y) which leads to the solution u = - 1/(8*pi*pi) * sin(2*pi*x) * sin(2*pi*y)

Hint: Stencil type must be changed to DMDA_STENCIL_BOX, coordinates vectors can be accessed using DMGetCoordinatesLocal

*/
#include <petsc.h>

int main(int argc, char *argv[])
{
  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));
  PetscCall(PetscFinalize());
  return 0;
}
