/*
  Given n, create two vectors v, w with size n and entries v[i] = n - i and w[i] = i.
  Create a third vector to hold their sum, and print the three vectors to screen.

  'n' Must be read from command line. The code should run in parallel for any number of processes, e.g.

     $ mpiexec -n 3 ./ex1 -n 5

  Remember to wrap calls within PetscCall to check for errors and to destroy the objects when no longer needed.
  Run with -info to have additional information on some of the low-level details.

  Hint: use PetscOptionsGetInt to read from command line and VecCreateMPI to create the vectors (see misc/vecscatter.c).
        Summation can be done in many different ways, see e.g. VecWAXPY or the other variants.
        VecSetValues insert entries and VecAssemblyBegin/End perform the needed communication of the values inserted.
*/
#include <petsc.h>

int main(int argc, char *argv[])
{
  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));
  PetscCall(PetscFinalize());
  return 0;
}
