/* Solve the second order ODE of the pendulum using a TS solver

   theta'' = - omega^2 * sin(theta)
   theta'(0) = 0
   theta(0) = theta0

   where omega is a parameter (corresponding to g/L, with g gravity and L the length of the pendulum).

   Read 'omega' and 'theta0' from command line.

   Introduce a new variable m = theta' and solve the nonlinear system of first order ODEs

            theta' = m
            m' = - omega^2 * sin(theta)

   Test the correctness of the solver by writing a monitoring routine that computes the Hamiltonian of the system

            H = 1/2 m^2 - omega^2 * cos(theta)

   that should remain constant.

   Write the Jacobian and check for its correctness using -snes_test_jacobian.

   Check the results for various values of omega and theta0 with a phase diagram.

   Experiment with different solvers.

   Hint: look at ../nonlinear/lotka_volterra.c for information on how to setup a TS solver.
*/
#include <petsc.h>

int main(int argc, char *argv[])
{
  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));
  PetscCall(PetscFinalize());
  return 0;
}
