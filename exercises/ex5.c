/* Solve the nonlinear system of equations

 (x - y^3 + 1)^3 - y^3 = 0
 x + 2y - 3 = 0

using a SNES solver. Take a look at ../nonlinear/bratu.c for additional information about how to setup a SNES solver.
Write a Jacobian callback and check for correctness using -snes_test_jacobian.
The system has [1,1] as solution.
Report the number of nonlinear iterations starting from 4 different initial guesses: [2,0], [2,2], [0,0] and [0,2].
*/
#include <petsc.h>

int main(int argc, char *argv[])
{
  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));
  PetscCall(PetscFinalize());
  return 0;
}
