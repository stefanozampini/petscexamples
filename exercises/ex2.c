/*
  Given n, create an n x n Hilbert matrix A, i.e. a_ij = 1/(i+j-1) for i,j=1,...n and a unsymmetric, banded Hilbert matrix B

            / 1/(i+j-1) if i-3 <= j <= i+2
     b_ij = |
            \ 0 otherwise

  Print the matrices and their 1 and infinity norms to stdout.

  Check the norms computations by writing your own norm function that
  multiplies the matrices with appropriate vectors internally.

  Hint: recall the definitions of matrix norms.
    - ||M||_1 = max(columnsum(abs(M)))
    - ||M||_infty = max(rowsum(abs(M)))
  and note that our matrices have all positive entries.

  Write a function the computes the trace of a matrix and check its correctness by
  dumping the values of the traces of A and B.
  Test to make sure that the input matrix is a square matrix.
  Hint: you can use MatGetDiagonal() to copy diagonal entries to a vector.

  'n' Must be read from command line. Use the matrix formats you think are appropriate.
  The code should run in parallel for any number of processes and inputs.
  Functions must return error codes. Make sure the code is bug- and leaks-free.
*/
#include <petsc.h>

int main(int argc, char *argv[])
{
  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));
  PetscCall(PetscFinalize());
  return 0;
}
