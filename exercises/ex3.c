/*
   Write a program that given an integer, a matrix type and a boolean solves the linear system A x = ones.
   The integer defines the size of the matrix.
   The boolean indicates the kind of matrix: if True, must be an Hilbert matrix, if False, a banded Hilbert
   matrix. See ex2.c for matrix definitions.
   For example

      ./ex3 -n 20 -mat_type dense -banded 1 -ksp_monitor

   must solve a 10x10 linear system with dense matrix format and banded Hilbert matrix,
   and report the convergence details.
   Check the result by computing the 2-norm of the residual ||A*x - b||_2.
   Try using different combinations of linear solvers and preconditioners.

   Hint: use MatSetFromOptions() to decide the matrix type, MatSetValues() to populate the matrix.
   Make sure you use the proper preallocation routines.
*/
#include <petsc.h>

int main(int argc, char *argv[])
{
  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));
  PetscCall(PetscFinalize());
  return 0;
}
