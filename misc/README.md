This folder contains examples on various PETSc objects that can be useful:

- [vecscatter](vecscatter.c): Showcase the VecScatter object and how it can be used to transparently perform general communications between PETSc vectors.

- [sf](sf.c): Showcase the PetscSF object and how it can be used to perform complicated communications patterns with arbitrary data-types.

- [sf_py](sf_py.py): An example Python script to communicate structured NumPy arrays with SF.
