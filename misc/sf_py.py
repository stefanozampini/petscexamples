# This script reproduces the 'struct' case described in sf.c

# Initialize PETSc with command line and import PETSc and MPI modules
import sys
import petsc4py

petsc4py.init(sys.argv)
from petsc4py import PETSc
from mpi4py import MPI
from mpi4py.util import dtlib
import numpy as np

rank = PETSc.COMM_WORLD.Get_rank()
size = PETSc.COMM_WORLD.Get_size()

# Access options database
OptDB = PETSc.Options()
nroots = OptDB.getInt('nroots', 5)
nleaves = OptDB.getInt('nroots', 6)

# Define NumPy struct dtype
# Here we are using Int and Real types from PETSc, as in sf.c
structdtype = np.dtype([('vint', PETSc.IntType()), ('rank', np.int32), ('vreal', PETSc.RealType())])

# Create MPI datatype using latest mpi4py facilities
mpidtype = dtlib.from_numpy_dtype(structdtype)
mpidtype.Commit()

# An alternative code would be
#
# blocklenghts = [1] * 3
# fields = [structdtype.fields[name] for name in structdtype.names]
# mpidatatypes = [MPI._typedict.get(name.char) for name, _ in fields]
# displacements = [d for _, d in fields]
# mpidtype = MPI.Datatype.Create_struct(blocklenghts, displacements, mpidatatypes)


# Create custom reduce operation
def _myOp(x, y, _mpidtype):
    # Need to pass from MPI.memoryBuffer to NumPy
    x = np.frombuffer(x, dtype=structdtype)
    y = np.frombuffer(y, dtype=structdtype)
    y['vint'] += x['vint']
    y['rank'] = np.maximum(x['rank'], y['rank'])
    y['vreal'] += x['vreal']


myOp = MPI.Op.Create(_myOp)

# Create SF and set graph
sf = PETSc.SF().create(comm=PETSc.COMM_WORLD)

remotes = np.empty((nleaves, 2), dtype=PETSc.IntType())
for i in range(nleaves):
    remotes[i][0] = (rank + i) % size
    remotes[i][1] = (i + 1) % nroots

sf.setGraph(nroots, None, remotes)
sf.setUp()

# Broadcast struct
leafdata = np.empty(nleaves, dtype=structdtype)
rootdata = np.empty(nroots, dtype=structdtype)
for i in range(nroots):
    rootdata[i] = (rank * nroots + i, rank, -(rank * nroots + i) / 2.0)

PETSc.Sys.Print('Broadcasting struct')
PETSc.Sys.syncPrint(f'[{rank}]:\n {rootdata.reshape(-1, 1)}', flush=True)
sf.bcastBegin(mpidtype, rootdata, leafdata, MPI.REPLACE)
sf.bcastEnd(mpidtype, rootdata, leafdata, MPI.REPLACE)
PETSc.Sys.Print('Received struct')
PETSc.Sys.syncPrint(f'[{rank}]:\n {leafdata.reshape(-1, 1)}', flush=True)

# Reduce struct
for i in range(nleaves):
    leafdata[i] = (1, rank, -1)
for i in range(nroots):
    rootdata[i] = (0, -1, 0)

PETSc.Sys.Print('Reducing struct with custom Op')
PETSc.Sys.syncPrint(f'[{rank}]:\n {leafdata.reshape(-1, 1)}', flush=True)
sf.reduceBegin(mpidtype, leafdata, rootdata, myOp)
sf.reduceEnd(mpidtype, leafdata, rootdata, myOp)
PETSc.Sys.Print('Reduced struct')
PETSc.Sys.syncPrint(f'[{rank}]:\n {rootdata.reshape(-1, 1)}', flush=True)
