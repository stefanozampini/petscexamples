/*
  In this example we show how to use the VecScatter API to perform general communications between PETSc vectors.
*/
#include <petscvec.h>

int main(int argc, char *argv[])
{
  PetscMPIInt rank, size;
  Vec         v, lv;
  VecType     vtype;
  IS          isto, isfrom;
  VecScatter  sct;
  PetscInt    n, ln, onlyrank, n_half;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));

  /* Get vector size from command line
     It will use the default value we set here if the option is
     not provided */
  n        = 4;
  onlyrank = -1;
  PetscCall(PetscOptionsGetInt(NULL, NULL, "-n", &n, NULL));
  PetscCall(PetscOptionsGetInt(NULL, NULL, "-onlyrank", &onlyrank, NULL));

  /* Create a parallel vector of size n. Here we use a different local size
     per process:
          if 0 <= onlyrank < size: (only rank == onlyrank gets the entire vector, the other processes have zero local size)
            rank == onlyrank -> localsize = n
            rank != onlyrank -> localsize = 0
          else: (default)
            let PETSc decide the local splitting
  */
  PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &rank));
  PetscCallMPI(MPI_Comm_size(PETSC_COMM_WORLD, &size));
  if (onlyrank < 0 || onlyrank >= size) ln = PETSC_DECIDE;
  else ln = (rank == onlyrank ? n : 0);
  PetscCall(VecCreateMPI(PETSC_COMM_WORLD, ln, n, &v));

  /* We cannot change the size at command line, but we can change the vector type with the option '-vec_type' */
  PetscCall(VecSetFromOptions(v));

  /* Set random values and view the vector */
  PetscCall(VecSetRandom(v, NULL));
  PetscCall(PetscPrintf(PetscObjectComm((PetscObject)v), "\nScattering to all\n"));
  PetscCall(VecView(v, NULL));

  /* Create a VecScatter object to perform communication between vectors.
     Here we use a predefined constructor that will send the entire parallel vector to each process */
  PetscCall(VecScatterCreateToAll(v, &sct, &lv));

  /* Perform the communications. This is a two stage process with Begin/End phases.
     This allows for computation to be overlapped with communication in between these two phases.
     In this case we will overwrite values at the destination and we will scatter from v to lv */
  PetscCall(VecScatterBegin(sct, v, lv, INSERT_VALUES, SCATTER_FORWARD));
  PetscCall(VecScatterEnd(sct, v, lv, INSERT_VALUES, SCATTER_FORWARD));

  /* Check on rank 0 */
  PetscCall(PetscPrintf(PetscObjectComm((PetscObject)v), "\nReceived on rank 0\n"));
  if (rank == 0) PetscCall(VecView(lv, NULL));

  /* Perform the reverse communication adding values together */
  PetscCall(VecSet(v, 0.0));
  PetscCall(VecSet(lv, rank + 1));
  PetscCall(VecScatterBegin(sct, lv, v, ADD_VALUES, SCATTER_REVERSE));
  PetscCall(VecScatterEnd(sct, lv, v, ADD_VALUES, SCATTER_REVERSE));

  PetscCall(PetscPrintf(PetscObjectComm((PetscObject)v), "\nReduced\n"));
  PetscCall(VecView(v, NULL));

  /* Cleanup */
  PetscCall(VecDestroy(&lv));
  PetscCall(VecScatterDestroy(&sct));

  /* Now we use the VecScatter API to create our own custom communication.
     In this example, we scatter from one parallel vector to another (irrespective of their parallel distribution!)
     We want to perform the following operation in NumPy notation : lv[-1:0:-1] = v[0::2]
  */
  PetscCall(PetscPrintf(PetscObjectComm((PetscObject)v), "\nScattering from v[0::2] to lv[-1:0:-1]\n"));

  /* We first create a second parallel vector with the same type of v and half the size.
     We do this programmatically using the Vec API */
  n_half = (n + 1) / 2; /* ceiling */
  PetscCall(VecCreate(PetscObjectComm((PetscObject)v), &lv));
  PetscCall(VecGetType(v, &vtype));
  PetscCall(VecSetType(lv, vtype));
  PetscCall(VecSetSizes(lv, PETSC_DECIDE, n_half)); /* let PETSc decide the parallel splitting */

  /* Create the index sets (IS) objects representing indices we scatter from and to
     Here we use the ISSTRIDE type to represent strided index sets */
  PetscCall(ISCreateStride(PetscObjectComm((PetscObject)v), n_half, 0, 2, &isfrom));         /* [0::2] */
  PetscCall(ISCreateStride(PetscObjectComm((PetscObject)v), n_half, n_half - 1, -1, &isto)); /* [-1:0:-1] */

  /* As all the other PETSc objects, we can inspect an IS with command line options */
  PetscCall(ISViewFromOptions(isfrom, NULL, "-isfrom_view"));
  PetscCall(ISViewFromOptions(isto, NULL, "-isto_view"));

  /* Create the VecScatter object that will perform the communication */
  PetscCall(VecScatterCreate(v, isfrom, lv, isto, &sct));

  /* Perform communications */
  PetscCall(VecSetRandom(v, NULL));
  PetscCall(VecView(v, NULL));
  PetscCall(VecScatterBegin(sct, v, lv, INSERT_VALUES, SCATTER_FORWARD));
  PetscCall(VecScatterEnd(sct, v, lv, INSERT_VALUES, SCATTER_FORWARD));
  PetscCall(PetscPrintf(PetscObjectComm((PetscObject)v), "\nReceived\n"));
  PetscCall(VecView(lv, NULL));
  PetscCall(PetscPrintf(PetscObjectComm((PetscObject)v), "\nReduced\n"));
  PetscCall(VecSet(v, 0.0));
  PetscCall(VecScatterBegin(sct, lv, v, ADD_VALUES, SCATTER_REVERSE));
  PetscCall(VecScatterEnd(sct, lv, v, ADD_VALUES, SCATTER_REVERSE));
  PetscCall(VecView(v, NULL));

  /* Cleanup */
  PetscCall(VecScatterDestroy(&sct));
  PetscCall(ISDestroy(&isto));
  PetscCall(ISDestroy(&isfrom));
  PetscCall(VecDestroy(&lv));
  PetscCall(VecDestroy(&v));
  PetscCall(PetscFinalize());
  return 0;
}
