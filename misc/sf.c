/*
  In this example we show how to use the PetscSF API to perform communications
  In short, PetscSF provides a simple API for managing common communication patterns in scientific computations by using a star-forest graph representation.
  For more details, see:

       Zhang, Junchao, et al. "The PetscSF scalable communication layer." IEEE Transactions on Parallel and Distributed Systems 33.4 (2021): 842-853
*/
#include <petscsf.h>

/* Include compatibility layer for older versions */
#include "../include/compat.h"

/* Define a type that we want to communicate/operate on with MPI */
typedef struct {
  PetscInt  vint;
  int       rank;
  PetscReal vreal;
} mystruct;

/* A custom MPI operation: sums the vint and vreal fields, takes the maximum of the rank field */
void MPIAPI mystruct_reduce(void *a, void *b, int *len, MPI_Datatype *datatype)
{
  PetscInt i, N = *len;

  for (i = 0; i < N; i++) {
    mystruct *A = (mystruct *)a;
    mystruct *B = (mystruct *)b;

    B->vint += A->vint;
    B->rank = PetscMax(A->rank, B->rank);
    B->vreal += A->vreal;
  }
}

/* If we want to use our custom struct with MPI, we need to create a MPI_Datatype and a MPI_Op.
   Note that MPIU_INT and MPIU_REAL are PETSc-defined MPI datatypes that are created by PETSc to
   operate with PetscInt and PetscReal respectively. Those are thus configuration agnostics and they allow
   our code to run in 32- or 64-bit indices and single- or double-precision configurations */
PetscErrorCode CreateMPIStruct(MPI_Datatype *MPIStructType, MPI_Op *MPIStructReduce)
{
  PetscMPIInt  blockLengths[3] = {1, 1, 1};
  MPI_Aint     blockOffsets[3] = {offsetof(mystruct, vint), offsetof(mystruct, rank), offsetof(mystruct, vreal)};
  MPI_Datatype blockTypes[3]   = {MPIU_INT, MPI_INT, MPIU_REAL};

  PetscFunctionBeginUser;
  PetscCallMPI(MPI_Type_create_struct(3, blockLengths, blockOffsets, blockTypes, MPIStructType));
  PetscCallMPI(MPI_Type_commit(MPIStructType));
  PetscCallMPI(MPI_Op_create(mystruct_reduce, PETSC_TRUE, MPIStructReduce));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* View the content of an array of structs. Here we use some API from PETSc
   to have a nicely-structured print, where all ranks with print in order */
PetscErrorCode MyStructView(PetscInt n, mystruct str[], PetscViewer viewer)
{
  PetscMPIInt rank;

  PetscFunctionBeginUser;
  PetscCallMPI(MPI_Comm_rank(PetscObjectComm((PetscObject)viewer), &rank));
  PetscCall(PetscViewerASCIIPushSynchronized(viewer));                    /* Tell the viewer we will print from many ranks */
  PetscCall(PetscViewerASCIISynchronizedPrintf(viewer, "[%d]:\n", rank)); /* Print -> queue the print */
  for (PetscInt i = 0; i < n; i++) {
    PetscCall(PetscViewerASCIISynchronizedPrintf(viewer, " (%d, %d, %g)", (int)str[i].vint, (int)str[i].rank, (double)str[i].vreal));
    PetscCall(PetscViewerASCIISynchronizedPrintf(viewer, "\n"));
  }
  PetscCall(PetscViewerFlush(viewer));                /* Flush the viewer */
  PetscCall(PetscViewerASCIIPopSynchronized(viewer)); /* Done with the sync print */
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* just a convenient macro for modulo operations */
#define MYMOD(a, b) ((a) % (b) < 0 ? (a) % (b) + (b) : (a) % (b))

int main(int argc, char *argv[])
{
  PetscMPIInt  rank, size;
  PetscInt     nleaves, nroots, *gidxs;
  PetscInt    *rootdataint, *leafdataint;
  mystruct    *rootdatastruct, *leafdatastruct;
  PetscSFNode *iremote;
  MPI_Datatype MPIStructType;
  MPI_Op       MPIStructReduce;
  PetscSF      sf;
  PetscLayout  layout;

  PetscCall(PetscInitialize(&argc, &argv, NULL, NULL));
  PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &rank));
  PetscCallMPI(MPI_Comm_size(PETSC_COMM_WORLD, &size));

  /* Command line customization */
  nroots = 5;
  PetscCall(PetscOptionsGetInt(NULL, NULL, "-nroots", &nroots, NULL));
  nleaves = 6;
  PetscCall(PetscOptionsGetInt(NULL, NULL, "-nleaves", &nleaves, NULL));

  /* With PetscSF, each process needs to specify only one-sided information:
     from where to take the data, in the form of an array of (rank, index) pairs.
     Here we setup an example of this communication pattern. Each process
     wants to get the ((i+1) % nroots)-th component from the ((rank + i) % size)-th process.
  */
  PetscCall(PetscMalloc1(nleaves, &iremote));
  for (PetscInt i = 0; i < nleaves; i++) {
    iremote[i].rank  = (rank + i) % size;
    iremote[i].index = (i + 1) % nroots;
  }
  PetscCall(PetscSFCreate(PETSC_COMM_WORLD, &sf));
  PetscCall(PetscSFSetGraph(sf, nroots, nleaves, NULL, PETSC_OWN_POINTER, iremote, PETSC_OWN_POINTER));
  PetscCall(PetscSFSetUp(sf));
  PetscCall(PetscSFViewFromOptions(sf, NULL, "-first_sf_view"));

  /* Allocate data we want to send and receive.
     PetscMalloc is just a convenience routine for malloc
     Here we call malloc once (with the proper amount of memory requested)
     and return 2 arrays, one for rootdataint, one for leafdataint */
  PetscCall(PetscMalloc2(nroots, &rootdataint, nleaves, &leafdataint));
  /* just fill with some unique value and print the values */
  for (PetscInt i = 0; i < nroots; i++) rootdataint[i] = rank * nroots + i;
  PetscCall(PetscIntView(nroots, rootdataint, PETSC_VIEWER_STDOUT_WORLD));

  /* Now we perform a broadcast operation, i.e. send data from roots to leaves and replace
     the content of leafdata. The Begin phase will pack the data,
     and issue the needed MPI send requests, while the End phase will complete the communication.
     This allows for computation to be overlapped with communication in between these two phases.
     Note that we can also use the same abstraction layer to send device (i.e. GPU) memory. */
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "\nBroadcasting ints\n"));
  PetscCall(PetscSFBcastBegin(sf, MPIU_INT, rootdataint, leafdataint, MPI_REPLACE));
  PetscCall(PetscSFBcastEnd(sf, MPIU_INT, rootdataint, leafdataint, MPI_REPLACE));

  /* Print the received data */
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Received ints\n"));
  PetscCall(PetscIntView(nleaves, leafdataint, PETSC_VIEWER_STDOUT_WORLD));

  /* We can also perform the reduce operation, i.e. from leaves to roots (the reverse of Bcast). */
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Reducing ints with sum\n"));
  for (PetscInt i = 0; i < nleaves; i++) leafdataint[i] = 1;
  for (PetscInt i = 0; i < nroots; i++) rootdataint[i] = 0;
  PetscCall(PetscSFReduceBegin(sf, MPIU_INT, leafdataint, rootdataint, MPI_SUM));
  PetscCall(PetscSFReduceEnd(sf, MPIU_INT, leafdataint, rootdataint, MPI_SUM));
  PetscCall(PetscIntView(nroots, rootdataint, PETSC_VIEWER_STDOUT_WORLD));
  PetscCall(PetscFree2(rootdataint, leafdataint));

  /* We can reuse the same PetscSF object to perform the same communication pattern with a different datatype,
     here we choose our own struct and custom reduce operation. */
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "\nBroadcasting struct\n"));
  PetscCall(CreateMPIStruct(&MPIStructType, &MPIStructReduce));
  PetscCall(PetscCalloc2(nroots, &rootdatastruct, nleaves, &leafdatastruct));
  for (PetscInt i = 0; i < nroots; i++) {
    rootdatastruct[i].vint  = rank * nroots + i;
    rootdatastruct[i].rank  = rank;
    rootdatastruct[i].vreal = -(rank * nroots + i) / 2.0;
  }
  PetscCall(MyStructView(nroots, rootdatastruct, PETSC_VIEWER_STDOUT_WORLD));
  PetscCall(PetscSFBcastBegin(sf, MPIStructType, rootdatastruct, leafdatastruct, MPI_REPLACE));
  PetscCall(PetscSFBcastEnd(sf, MPIStructType, rootdatastruct, leafdatastruct, MPI_REPLACE));
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Received struct\n"));
  PetscCall(MyStructView(nleaves, leafdatastruct, PETSC_VIEWER_STDOUT_WORLD));
  for (PetscInt i = 0; i < nleaves; i++) {
    leafdatastruct[i].vint  = 1;
    leafdatastruct[i].rank  = rank;
    leafdatastruct[i].vreal = -1.0;
  }
  for (PetscInt i = 0; i < nroots; i++) {
    rootdatastruct[i].vint  = 0;
    rootdatastruct[i].rank  = -1;
    rootdatastruct[i].vreal = 0.0;
  }
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Reducing struct with custom operation\n"));
  PetscCall(PetscSFReduceBegin(sf, MPIStructType, leafdatastruct, rootdatastruct, MPIStructReduce));
  PetscCall(PetscSFReduceEnd(sf, MPIStructType, leafdatastruct, rootdatastruct, MPIStructReduce));
  PetscCall(MyStructView(nroots, rootdatastruct, PETSC_VIEWER_STDOUT_WORLD));
  PetscCall(PetscFree2(rootdatastruct, leafdatastruct));

  /* If we set a new graph the SF is reset and a new communication pattern is created.
     Here we use a predefined API that customizes an SF by using a parallel layout and a global index
     of the data to be received, a common communication pattern in partial differential equations.
     In this example we specify global indices as (in NumPy notation):
              np.arange(3*(size - rank) - 1, 3*(size - rank) - nleaves - 1, -1)
  */
  /* We first setup a PetscLayout that stores the information needed to represent parallel vectors
     and matrices, with local chunks on each process */
  PetscCall(PetscLayoutCreate(PETSC_COMM_WORLD, &layout));
  PetscCall(PetscLayoutSetLocalSize(layout, 3)); /* each rank will have three local entries */
  PetscCall(PetscLayoutSetUp(layout));

  /* Then define the global indices we would like to receive from */
  PetscCall(PetscMalloc1(nleaves, &gidxs));
  for (PetscInt i = 0; i < nleaves; i++) gidxs[i] = MYMOD(3 * (size - rank) - i - 1, 3 * size);
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "\nBroadcasting ints on layout sf\n"));
  PetscCall(PetscIntView(nleaves, gidxs, PETSC_VIEWER_STDOUT_WORLD));

  /* Customize the SF */
  PetscCall(PetscSFSetGraphLayout(sf, layout, nleaves, NULL, PETSC_OWN_POINTER, gidxs));
  PetscCall(PetscSFSetUp(sf));
  PetscCall(PetscSFViewFromOptions(sf, NULL, "-second_sf_view"));

  /* These are no longer needed */
  PetscCall(PetscLayoutDestroy(&layout));
  PetscCall(PetscFree(gidxs));

  /* Use PetscSF API to inquire about the number of roots */
  PetscCall(PetscSFGetGraph(sf, &nroots, NULL, NULL, NULL));

  /* Broadcast (from roots to leaves) int data */
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Broadcasting ints\n"));
  PetscCall(PetscCalloc2(nroots, &rootdataint, nleaves, &leafdataint));
  for (PetscInt i = 0; i < nroots; i++) rootdataint[i] = rank * nroots + i;
  PetscCall(PetscIntView(nroots, rootdataint, PETSC_VIEWER_STDOUT_WORLD));
  PetscCall(PetscSFBcastBegin(sf, MPIU_INT, rootdataint, leafdataint, MPI_REPLACE));
  PetscCall(PetscSFBcastEnd(sf, MPIU_INT, rootdataint, leafdataint, MPI_REPLACE));
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Received ints\n"));
  PetscCall(PetscIntView(nleaves, leafdataint, PETSC_VIEWER_STDOUT_WORLD));

  /* Reduce (from leaves to roots) int data with sum operation */
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Reducing ints with sum\n"));
  for (PetscInt i = 0; i < nleaves; i++) leafdataint[i] = 1;
  for (PetscInt i = 0; i < nroots; i++) rootdataint[i] = 0;
  PetscCall(PetscSFReduceBegin(sf, MPIU_INT, leafdataint, rootdataint, MPI_SUM));
  PetscCall(PetscSFReduceEnd(sf, MPIU_INT, leafdataint, rootdataint, MPI_SUM));
  PetscCall(PetscIntView(nroots, rootdataint, PETSC_VIEWER_STDOUT_WORLD));
  PetscCall(PetscFree2(rootdataint, leafdataint));

  /* Cleanup */
  PetscCallMPI(MPI_Type_free(&MPIStructType));
  PetscCallMPI(MPI_Op_free(&MPIStructReduce));
  PetscCall(PetscSFDestroy(&sf));
  PetscCall(PetscFinalize());
  return 0;
}
